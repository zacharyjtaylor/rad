#`c` is the compiler used. Defaulting to dmd.
ifneq ($(c), gdc)
dmd: RAD_Main.d
	dmd *.d -of=RAD
else
gdc: RAD_Main.d
	gdc *.d -oRAD
endif
