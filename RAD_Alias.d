module RAD_Alias;
//A module for APL-style single-character aliases.

dstring dealias(dstring arg) {
	dstring[dstring] aliases = [
		"⍺"d: "α"d,
		"⍳"d: "ι"d,
		"⍴"d: "ρ"d,
		"⍵"d: "ω"d
	];
	if(arg in aliases) return aliases[arg];
	return arg;
}
