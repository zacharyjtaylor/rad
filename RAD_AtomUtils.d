module RAD_AtomUtils;
//Utilities for atoms

import RAD_Value, RAD_utils, RAD_Variable;
import std.functional: toDelegate;
import std.math, std.conv, std.stdio;
import std.algorithm.iteration: fold;
import std.algorithm.iteration: cumulativeFold;
import std.array: array;

dstring removeNewline(dstring arg) { //Thanks TIO for making me catch this error
	if(arg[$ - 1] == dchar('\n')) {
		return arg[0 .. $-1];
	}
	return arg;
}

alias F = Value delegate(Value[]);

int depth(Value_ arg) { //FIXME: TOO DANG SLOW
	if(arg.peek!(Value_[]) is null) {
		return 0;
	}
	int tmp = 1;
	foreach(i; arg.get!(Value_[])) {
		int x = depth(i) + 1;
		if(x > tmp) tmp = x;
	}
	return tmp;
}
int depth(Value arg) {
	return depth(arg.theValue);
}

Value vectorizeMonadic(F f, int d, Value omega) { //vectorizes `f` to depth `d`
	if(d == -1 || depth(omega) <= d) {
		return f([omega]);
	}
	Value[] res;
	foreach(el; omega.get!(Value_[])) {
		res ~= vectorizeMonadic(f, d, Value(el));
	}
	return Value(res);
}

Value vectorizeDyadic(F f, int dl, int dr, Value alpha, Value omega) { //vectories to the left at `dl` and right at `dr`
	if((dl == -1 || depth(alpha) <= dl) && (dr == -1 || depth(omega) <= dr)) {
		return f([alpha, omega]);
	}
	if(dl == -1 || depth(alpha) <= dl) {
		return vectorizeMonadic(delegate Value(Value[] arg) {
			return f([alpha, arg[0]]);
		}, dr, omega);
	}
	if(dr == -1 || depth(omega) <= dr) {
		return vectorizeMonadic(delegate Value(Value[] arg) {
			return f([arg[0], omega]);
		}, dl, alpha);
	}
	if(alpha.length == omega.length) {
		Value[] res;
		for(ulong i = 0; i < alpha.length; i++) {
			res ~= vectorizeDyadic(f, dl, dr, alpha[i], omega[i]);
		}
		return Value(res);
	}
	throw new Exception("RAD: NYI (MISMATCH)");
}

F vectorize(F f, int mon, int alp, int omg) { //Vectorizes functions (both monadic + dyadic in one function)
	//FIXME: allow for nonvectorization of a case
	return delegate Value(Value[] arg) {
		if(arg.length == 1) {
			return vectorizeMonadic(f, mon, arg[0]);
		}
		if(arg.length == 2) {
			return vectorizeDyadic(f, alp, omg, arg[0], arg[1]);
		}
		throw new Exception("RAD: INTERNAL ERROR");
	};
}

F vectorize(Value function(Value[]) f, int mon, int alp, int omg) {
	return vectorize(toDelegate(f), mon, alp, omg);
}

Value standardize(F f) { //Allow trains to work
	return Value(delegate Value(Value[] args) { //inner function
		if(args.length == 1 && args[0].peek!Function is null) { //f A
			return f(args);
		}
		if(args.length == 1 && args[0].peek!Function !is null) { //f g
			return Value(delegate Value(Value[] argv) {
				return f([args[0](argv)]);
			});
		}
		if(args.length == 2 && args[0].peek!Function is null && args[1].peek!Function is null) { //A f B
			return f(args);
		}
		if(args.length == 2 && args[0].peek!Function is null && args[1].peek!Function !is null) { //A f g
			return Value(delegate Value(Value[] argv) {
				return f([args[0], args[1](argv)]);
			});
		}
		if(args.length == 2 && args[0].peek!Function !is null && args[1].peek!Function is null) { //g f A (`g` must &'d)
			return Value(delegate Value(Value[] argv) {
				return f([args[0](argv), args[1]]);
			});
		}
		if(args.length == 2 && args[0].peek!Function !is null && args[1].peek!Function !is null) { //g f h
			return Value(delegate Value(Value[] argv) {
				return f([args[0](argv), args[1](argv)]);
			});
		}
		throw new Exception("RAD: INTERNAL ERROR");
	});
}
Value standardize(Value function(Value[]) f) {
	return standardize(toDelegate(f));
}


bool isRealValue(Value arg) { //Make sure we have real numbers
	return arg.peek!Number !is null && arg.get!Number.im == 0;
}
bool isIntValue(Value arg) { //Make sure it's an integer
	return isRealValue(arg) && arg.get!Number.re.to!int == arg.get!Number.re;
}

Value enumerate(Value arg, int[] prefix = []) { //A version of `ε` for D.
	Value[] res;
	if(arg.peek!(Value_[]) is null) {
		return Value(prefix);
	}
	for(int i; i < arg.get!(Value_[]).length; i++) {
		res ~= enumerate(arg[i], prefix ~ (i + getIO().get!Number.re.to!int));
	}
	return Value(res);
}

Value setIndex(Value arg, Value ind, Value val) { //used for indexed assignment: `ind` uses the current IO
	if(ind.peek!(Value_[]) is null) {
		throw new Exception("RAD: INTERNAL ERROR");
	}
	if(ind.get!(Value_[]).length == 0) {
		return val;
	}
	Value j = ind[0] - getIO(); //D's index.
	Value s = ind.get!(Value_[]).length > 1 ? Value(ind.get!(Value_[])[1 .. $]) : Value([]);
	Value[] res;
	for(int i; i < arg.get!(Value_[]).length; i++) {
		if(Value(i) == j) {
			res ~= setIndex(arg[i], s, val);
		} else {
			res ~= arg[i];
		}
	}
	return Value(res);
}

dstring repr(Value arg, bool output = false) { //`output == false`: implicit output representation, `output == true`: explicit output representation. 
	if(arg.peek!Number !is null) {
		double realPart = arg.get!Number.re;
		double imagPart = arg.get!Number.im;

		if(realPart == 0 && imagPart == 0) {
			return "0"d;
		}

		dstring rp;
		dstring ip;

		if(realPart == 0) {
			rp = "";
		} else if(realPart == floor(realPart)) {
			rp = dtext(abs(to!int(realPart)));
		} else {
			rp = dtext(abs(realPart));
		}
		if(realPart != 0 && realPart < 0) {
			rp = "¯"d ~ rp;
		}

		if(imagPart == 0) {
			ip = "";
		} else if(imagPart == floor(imagPart)) {
			ip = dtext(abs(to!int(imagPart)));
		} else {
			ip = dtext(abs(imagPart));
		}
		if(realPart != 0 && imagPart != 0 && imagPart < 0) {
			ip = "-"d ~ ip;
		} else if(realPart != 0 && imagPart != 0) {
			ip = "+"d ~ ip;
		} else if(imagPart != 0 && imagPart < 0) {
			ip = "¯"d ~ ip;
		}
		if(imagPart != 0) {
			ip = ip ~ "i"d;
		}

		if(realPart != 0 && imagPart != 0) {
			
			return "("d ~ rp ~ ip ~ ")"d;
		}
		return rp ~ ip;
	}
	if(arg.peek!dchar !is null) {
		if(output) return arg.get!dchar.dtext;
		else return "'"d ~ arg.get!dchar.dtext ~ "'"d;
	}
	if(arg.peek!(Value_[]) !is null) {
		if(arg.get!(Value_[]).length == 0) {
			return "⍬"d;
		}
		if(output && !arg.get!(Value_[]).map(delegate bool(Value_ inner) {
				return inner.peek!dchar is null;
		}).contains(true)) {
			dstring tmp = ""d;
			foreach(i; arg.get!(Value_[])) {
				tmp ~= i.get!dchar;
			}
			return tmp;
		}
		if(arg.get!(Value_[]).length == 1) {
			return "(⊂"d ~ repr(Value(arg[0]), output) ~ ")"d;
		}
		dstring tmp = "("d;
		foreach(el; arg.get!(Value_[])) {
			tmp ~= repr(Value(el), output);
			tmp ~= " "d;
		}
		return tmp[0 .. $ - 1] ~ ")"d;
	}
	return (cast(dstring)arg);
}

Value[] repeat(Value item, int amount) {
	Value[] res = [];
	for(int i = 0; i < amount; i++) {
		res ~= [item];
	}
	return res;
}

Value blank(Value shape) { //construct an array of zeros that has shape `shape`
	if(shape == Value([])) {
		return Value(0);
	}
	if(isRealValue(shape)) {
		return Value(repeat(Value(0), shape.get!Number.re.to!int));
	}
	if(shape.peek!(Value_[]) !is null) {
		Value[] res;
		ulong i = shape.get!(Value_[]).length - 1;
		while(i >= 0 && i <= shape.get!(Value_[]).length - 1) {
			if(i != 0 && shape[i].peek!(Value_[]) !is null && isRealValue(shape[i - 1])) {
				res ~= Value(shape[i].get!(Value_[]).map(delegate Value(Value_ j) {
					return blank(Value(j));
				}));
				i -= 2;
			} else if(isRealValue(shape[i])) {
				if(res.length == 0) {
					res = repeat(Value(0), shape[i].get!Number.re.to!int);
				} else {
					res = repeat(Value(res), shape[i].get!Number.re.to!int);
				}
				i -= 1;
			} else {
				i -= 1;
			}
		}
		return Value(res);
	}
	throw new Exception("RAD: DOMAIN ERROR");
}

Value mold(Value shape, Value content, ref ulong index) { //mold `content` into the shape of `shape`
	Value res = Value(shape);
	Value_[] arr = res.get!(Value_[]);
	for(ulong i; i < arr.length; i++) {
		if(arr[i].peek!(Value_[]) !is null) {
			res[i] = mold(res[i], content, index);
		} else {
			res[i] = content[index];
			index = (index + 1) % content.get!(Value_[]).length;
		}
	}
	return res;
}

F toFn(Value arg) { //Used for reductions and scans
	return delegate Value(Value[] args) {
		return arg(args);
	};
}

Value RTLReduce(F f, Value arg) {
	if(arg.peek!(Value_[]) is null) {
		return arg;
	}
	if(arg.get!(Value_[]).length == 0) {
		throw new Exception("RAD: NYI ERROR");
	}
	if(arg.get!(Value_[]).length == 1) {
		return arg[0];
	}
	return f([arg[0], RTLReduce(f, Value(arg.get!(Value_[])[1 .. $]))]);
}

Value RTLScan(F f, Value arg) {
	if(arg.peek!(Value_[]) is null) {
		return Value([arg]);
	}
	if(arg.get!(Value_[]).length == 0) {
		return Value([]);
	}
	if(arg.get!(Value_[]).length == 1) {
		return arg;
	}
	Value[] res;
	for(ulong i = 1; i <= arg.get!(Value_[]).length; i++) {
		res ~= RTLReduce(f, Value(arg.get!(Value_[])[0 .. i]));
	}
	return Value(res);
}

Value LTRReduce(F f, Value arg) {
	if(arg.peek!(Value_[]) is null) {
		return arg;
	}
	if(arg.get!(Value_[]).length == 0) {
		throw new Exception("RAD: NYI ERROR");
	}
	if(arg.get!(Value_[]).length == 1) {
		return arg[0];
	}
	return Value(arg.get!(Value_[]).fold!(delegate Value(Value_ a, Value_ b) => f([Value(a), Value(b)]))());
}

Value LTRScan(F f, Value arg) {
	if(arg.peek!(Value_[]) is null) {
		return Value([arg]);
	}
	if(arg.get!(Value_[]).length == 0) {
		return Value([]);
	}
	if(arg.get!(Value_[]).length == 1) {
		return arg;
	}
	Value[] res;
	for(ulong i = 1; i <= arg.get!(Value_[]).length; i++) {
		res ~= LTRReduce(f, Value(arg.get!(Value_[])[0 .. i]));
	}
	return Value(res);
}

Value prototype(Value arg) {
	if(arg.peek!Number !is null) {
		return Value(0);
	}
	if(arg.peek!dchar !is null) {
		return Value(dchar(' '));
	}
	if(arg.peek!(Value_[]) !is null) {
		foreach(i; arg.get!(Value_[])) {
			if(prototype(Value(i)) == Value(0)) {
				return Value(0);
			}
		}
		if(arg.get!(Value_[]).length == 0) {
			return Value(0);
		}
		return Value(dchar(' '));
	}
	throw new Exception("RAD: DOMAIN ERROR");
}

double getDouble(Value_ arg) {
	if(arg.peek!Number !is null) {
		return arg.get!Number.re;
	}
	if(arg.peek!dchar !is null) {
		return arg.get!dchar.to!int.to!double;
	}
	throw new Exception("RAD: DOMAIN ERROR");
}
