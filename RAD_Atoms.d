module RAD_Atoms;

import std.complex, std.conv, std.stdio, std.mathspecial, std.math, std.numeric;
import std.functional : toDelegate;
import std.algorithm.iteration : reduce;
import RAD_LazyCall, RAD_Value, RAD_AtomUtils, RAD_utils, RAD_Variable;
import std.algorithm.sorting : sort;
import std.array : array;

LazyCall lowercaseAlpha() { //α: #00
	return new LazyCall(delegate Value(Value[] args) {
		return getVariable_("α"d);
	}, [], "α"d);
}
	
Value lowercaseEpsilon(Value[] arg) { //ε: #04
	if(arg.length == 1) {
		return enumerate(arg[0]);
	}
	if(arg.length == 2) {
		throw new Exception("RAD: NYI ERROR");
	}
	throw new Exception("RAD: INTERNAL ERROR");
}

Value lowercaseIota(Value[] arg) { //ι: #08
	if(arg.length == 1) {
		if(isRealValue(arg[0])) {
			Value[] res;
			for(int i = 0; i < arg[0].get!Number.re; i++) {
				res ~= getIO() + i;
			}
			return Value(res);
		}
		if(arg[0].peek!(Value_[]) !is null) {
			if(arg[0].get!(Value_[]).length == 0) {
				return Value([]);
			}
			if(arg[0].get!(Value_[]).length == 1) {
				return lowercaseIota([arg[0][0]]);
			}
			Value S = lowercaseIota([Value(arg[0].get!(Value_[])[$-1])]);
			Value T = lowercaseIota([Value(arg[0].get!(Value_[])[0 .. $-1])]);
			return Value(T.get!(Value_[]).map(delegate Value(Value_ y) {
				return Value(S.get!(Value_[]).map(delegate Value(Value_ x) {
					return Value([y, x]);
				}));
			}));
		}
		throw new Exception("RAD: DOMAIN ERROR");
	}
	if(arg.length == 2) {
		if(arg[0].peek!(Value_[]) !is null) {
			if(arg[1].peek!(Value_[]) !is null) {
				Value[] res;
				Value_[] arr = arg[0].get!(Value_[]);
				foreach(el; arg[1].get!(Value_[])) {
					res ~= Value(arr.indexOf(el)) + getIO();
				}
				return Value(res);
			}
			return Value(arg[0].get!(Value_[]).indexOf(arg[1].theValue) + getIO());
		}
		throw new Exception("RAD: DOMAIN ERROR");
	}
	throw new Exception("RAD: INTERNAL ERROR");
}

Value lowercaseMu(Value[] arg) { //μ: #0C
	if(arg.length == 1) {
		if(arg[0].peek!(Value_[]) is null) {
			return arg[0];
		}
		Value res = Value(0);
		foreach(i; arg[0].get!(Value_[])) {
			res = res + Value(i);
		}
		return arg[0].get!(Value_[]).length > 0 ? res / arg[0].get!(Value_[]).length : Value(0);
	}
	if(arg.length == 2) {
		ulong tmp = 0;
		return mold(arg[1], arg[0], tmp);
	}
	throw new Exception("RAD: INTERNAL ERROR");
}

double lowercasePi = PI; //π: #0E

Value lowercaseRho(Value[] args) { //ρ: #0F
	if(args.length == 1) {
		Value arg = args[0];
		if(arg.peek!(Value_[]) !is null && !arg.get!(Value_[]).map(delegate bool(Value_ inner) {
				return inner.peek!(Value_[]) !is null;
		}).contains(true)) {
			return Value([arg.get!(Value_[]).length]);
		}
		if(arg.peek!(Value_[]) !is null) {
			Value[] res = arg.get!(Value_[]).map(delegate Value(Value_ inner) {
				return lowercaseRho([Value(inner)]);
			});
			bool flag = true;
			foreach(i; res) {
				if(i != res[0]) {
					flag = false;
				}
			}
			if(flag) {
				return Value([Value(arg.get!(Value_[]).length)] ~ res[0].get!(Value_[]).map(delegate Value(Value_ i) {
					return Value(i);
				}));
			}
			return Value([Value(arg.get!(Value_[]).length)] ~ [Value(res.map(delegate Value(Value i) {
				return i.get!(Value_[]).length == 1 && i[0].peek!(Value_[]) is null ? i[0] : i;
			}))]);
		}
		return Value([]);
	}
	if(args.length == 2) {
		Value shape = blank(args[0]);
		Value content = memberOf([args[1]]);
		ulong index = 0;
		return mold(shape, content, index);
	}
	throw new Exception("RAD: INTERNAL ERROR");
}

LazyCall lowercaseOmega() { //ω: #15
	return new LazyCall(delegate Value(Value[] args) {
		return getVariable_("ω"d);
	}, [], "ω"d);
}

Value exclamationMark(Value[] arg) { //!: #21
	if(arg.length == 1) {
		if(isRealValue(arg[0])) {
			return Value(gamma(arg[0].get!Number.re + 1));
		}
		throw new Exception("RAD: DOMAIN ERROR");
	}
	if(arg.length == 2) {
		if(isRealValue(arg[0]) && isRealValue(arg[1])) {
			return Value(gamma(arg[1].get!Number.re + 1) / gamma(arg[0].get!Number.re + 1) / gamma(arg[1].get!Number.re - arg[0].get!Number.re + 1));
		}
		throw new Exception("RAD: DOMAIN ERROR");
	}
	throw new Exception("RAD: INTERNAL ERROR");
}

Value quotationMark(Value[] arg) { //": #22
	if(arg.length == 1) {
		if(isIntValue(arg[0])) {
			return Value(dchar(arg[0].get!Number.re.to!int));
		}
		if(arg[0].peek!dchar !is null) {
			return Value(int(arg[0].get!dchar));
		}
		throw new Exception("RAD: DOMAIN ERROR");
	}
	if(arg.length == 2) {
		throw new Exception("RAD: NYI ERROR");
	}
	throw new Exception("RAD: INTERNAL ERROR");
}

Value ampersand(Value[] arg) {//&: #26
	if(arg.length == 1) {
		return standardize(delegate Value(Value[] args) {
			if(args.length == 1) {
				return arg[0];
			}
			if(args.length == 2) {
				return arg[0];
			}
			throw new Exception("RAD: INTERNAL ERROR");
		});
	}
	throw new Exception("RAD: INTERNAL ERROR");
}

Value asterisk(Value[] arg) { //*: #2A
	if(arg.length == 1) {
		if(arg[0].peek!Number !is null) {
			return Value(E) ^^ arg[0].get!Number;
		}
		throw new Exception("RAD: DOMAIN ERROR");
	}
	if(arg.length == 2) {
		if(arg[0].peek!Number !is null && arg[1].peek!Number !is null) {
			return arg[0] ^^ arg[1];
		}
		throw new Exception("RAD: DOMAIN ERROR");
	}
	throw new Exception("RAD: INTERNAL ERROR");
}

Value plusSign(Value[] arg) { //+: #2B
	if(arg.length == 1) {
		if(arg[0].peek!Number !is null) {
			return Value(conj(arg[0].get!Number));
		}
		throw new Exception("RAD: DOMAIN ERROR");
	}
	if(arg.length == 2) {
		if(arg[0].peek!Number !is null && arg[1].peek!Number !is null) {
			return arg[0] + arg[1];
		}
		throw new Exception("RAD: DOMAIN ERROR");
	}
	throw new Exception("RAD: INTERNAL ERROR");
}

Value comma(Value[] arg) { //,: #2C
	if(arg.length == 1) {
		if(arg[0].peek!(Value_[]) is null) {
			return Value(arg);
		}
		return fSlash([Value(&comma)])(arg);
	}
	if(arg.length == 2) {
		if(arg[0].peek!(Value_[]) !is null) {
			return arg[0] ~ arg[1];
		}
		if(arg[1].peek!(Value_[]) !is null) {
			return Value([arg[0]]) ~ arg[1];
		}
		return Value(arg);
	}
	throw new Exception("RAD: INTERNAL ERROR");
}

Value hyphenMinus(Value[] arg) { //-: #2D
	if(arg.length == 1) {
		if(arg[0].peek!Number !is null) {
			return -arg[0];
		}
		throw new Exception("RAD: DOMAIN ERROR");
	}
	if(arg.length == 2) {
		if(arg[0].peek!Number !is null && arg[1].peek!Number !is null) {
			return arg[0] - arg[1];
		}
		throw new Exception("RAD: DOMAIN ERROR");
	}
	throw new Exception("RAD: INTERNAL ERROR");
}

Value fSlash(Value[] arg) { ///: #2F
	if(arg.length == 1) {
		if(arg[0].peek!Function !is null) {
			return standardize(delegate Value(Value[] args) {
				if(args.length == 1) {
					return RTLReduce(toFn(arg[0]), args[0]);
				}
				if(args.length == 2) {
					throw new Exception("RAD: NYI ERROR");
				}
				throw new Exception("RAD: INTERNAL ERROR");
			});
		}
		throw new Exception("RAD: NYI ERROR");
	}
	throw new Exception("RAD: INTERNAL ERROR");
}
			
Value semicolon(Value[] arg) { //;: #3B
	Number NAN = Number(double.nan, double.nan);
	if(arg.length == 0) return Value(new Semicolon(Value(NAN), Value(NAN)));
	if(arg.length == 2) return Value(new Semicolon(arg[0], arg[1]));
	throw new Exception("RAD: INTERNAL ERROR");
}

Value lessThan(Value[] arg) { //<: #3C
	if(arg.length == 1) {
		if(arg[0].peek!(Value_[]) is null) {
			return Value(arg);
		}
		return Value(arg[0].get!(Value_[])().sort!((a, b) => getDouble(a) < getDouble(b))().array);
	}
	if(arg.length == 2) {
		if(isRealValue(arg[0]) && isRealValue(arg[1])) {
			return Value(int(arg[0].get!Number.re < arg[1].get!Number.re));
		}
		throw new Exception("RAD: DOMAIN ERROR");
	}
	throw new Exception("RAD: INTERNAL ERROR");
}

Value equalTo(Value[] arg) { //=: #3D
	if(arg.length == 1) {
		throw new Exception("RAD: NYI ERROR");
	}
	if(arg.length == 2) {
		return Value(int(arg[0] == arg[1]));
	}
	throw new Exception("RAD: INTERNAL ERROR");
}

Value greaterThan(Value[] arg) { //>: #3E
	if(arg.length == 1) {
		if(arg[0].peek!(Value_[]) is null) {
			return Value(arg);
		}
		return Value(arg[0].get!(Value_[])().sort!((a, b) => getDouble(a) > getDouble(b))().array);
	}
	if(arg.length == 2) {
		if(isRealValue(arg[0]) && isRealValue(arg[1])) {
			return Value(int(arg[0].get!Number.re > arg[1].get!Number.re));
		}
		throw new Exception("RAD: DOMAIN ERROR");
	}
	throw new Exception("RAD: INTERNAL ERROR");
}

Value questionMark(Value[] arg) { //?: #3F
	import std.random;
	if(arg.length == 1) {
		if(arg[0] == Value(0)) {
			return Value(uniform!("[)", double, double)(0.0, 1.0));
		}
		if(isIntValue(arg[0]) && arg[0].get!Number.re.to!int > 0) {
			return getIO() + Value(uniform!("[)", int, int)(0, arg[0].get!Number.re.to!int));
		}
		throw new Exception("RAD: DOMAIN ERROR");
	}
	if(arg.length == 2) {
		if(isIntValue(arg[0]) && isIntValue(arg[1])) {
			Value_[] arr = lowercaseIota([arg[1]]).get!(Value_[]);
			ulong end = arg[0].get!Number.re.to!ulong;
			arr.randomShuffle(); //GDC specific weird stuff again: older version of D have this as `void` (including the GDC I have).
			return Value(arr[0 .. end]);
		}
		throw new Exception("RAD: DOMAIN ERROR");
	}
	throw new Exception("RAD: INTERNAL ERROR");
}

Value indexBrackets(Value[] arg) { //[: #5B; ]: #5D
	//I've been struggling to get this to work
	//I directly ported the Python version.
	Value A = arg[0];
	Value B = arg[1];
	if(B.peek!(Value_[]) !is null) {
		return vectorizeMonadic(delegate Value(Value[] i) {
			return A[i[0] - getIO()];
		}, 0, B);
	}
	if(B.peek!Semicolon is null) {
		return A[B - getIO()];
	}
	Semicolon y = B.get!Semicolon.flatten().reduce!((a, b) => Value(new Semicolon(a, b))).get!Semicolon; //This is to align it with the Python version.
	if(y.A.peek!(Value_[]) is null) {
		return indexBrackets([indexBrackets([A, y.A]), y.B]);
	}
	Value A_ = indexBrackets([A, y.A]);
	return Value(A_.get!(Value_[]).map(delegate Value(Value_ w) {
		return indexBrackets([Value(w), y.B]);
	}));	
	
}

Value bSlash(Value[] arg) { //\: #5C
	if(arg.length == 1) {
		if(arg[0].peek!Function !is null) {
			return standardize(delegate Value(Value[] args) {
				if(args.length == 1) {
					return RTLScan(toFn(arg[0]), args[0]);
				}
				if(args.length == 2) {
					throw new Exception("RAD: NYI ERROR");
				}
				throw new Exception("RAD: INTERNAL ERROR");
			});
		}
		throw new Exception("RAD: NYI ERROR");
	}
	throw new Exception("RAD: INTERNAL ERROR");
}

Value verticalLine(Value[] arg) { //|: #7C
	if(arg.length == 1) {
		if(arg[0].peek!Number !is null) {
			return Value(std.complex.abs(arg[0].get!Number));
		}
		throw new Exception("RAD: DOMAIN ERROR");
	}
	if(arg.length == 2) {
		if(arg[0].peek!Number !is null && arg[0].peek!Number !is null) {
			return arg[1] % arg[0];
		}
		throw new Exception("RAD: DOMAIN ERROR");
	}
	throw new Exception("RAD: INTERNAL ERROR");
}

Value tilde(Value[] arg) { //~: #7E
	if(arg.length == 1) {
		if(arg[0].peek!Number !is null) {
			return Value(int(arg[0].get!Number == Number(0)));
		}
		if(arg[0].peek!dchar !is null) {
			return Value(int(arg[0].get!dchar == dchar('\x00')));
		}
		return Value(0);
	}
	if(arg.length == 2) {
		if(arg[1].peek!(Value_[]) is null) {
			return tilde([arg[0], Value([arg[1]])]);
		}
		if(arg[0].peek!(Value_[]) !is null) {
			Value[] res;
			foreach(i; arg[0].get!(Value_[])) {
				if(!arg[1].get!(Value_[])().contains(i)) {
					res ~= Value(i);
				}
			}
			return Value(res);
		}
		throw new Exception("RAD: DOMAIN ERROR");
	}
	throw new Exception("RAD: INTERNAL ERROR");
}

Value[] zilde = []; //⍬: #82

Value rightTack(Value[] arg) { //⊢: #83
	return arg[$ - 1];
}

Value leftTack(Value[] arg) { //⊣: #86
	return arg[0];
}

Value squishQuad(Value[] arg) { //⌷: #87
	if(arg.length == 1) {
		Value shape = lowercaseRho(arg);
		shape = lowercaseRho([shape]);
		shape = lowercaseRho([shape]);
		return Value(int(Value([1]) == shape));
	}
	if(arg.length == 2) {
		Value ind = arg[0].peek!(Value_[]) is null ? Value([arg[0]]) : arg[0];
		Value res = arg[1];
		foreach(i; ind.get!(Value_[])) {
			res = res[Value(i) - getIO()];
		}
		return res;
	}
	throw new Exception("RAD: INTERNAL ERROR");
}

Value diaeresis(Value[] arg) { //¨: #88
	if(arg.length == 1) {
		if(arg[0].peek!Function !is null) {
			Value fn = arg[0];
			return standardize(delegate Value(Value[] args) {
				if(args.length == 1) {
					Value x = args[0].peek!(Value_[]) is null ? Value([args[0]]) : args[0];
					Value[] res;
					foreach(el; x.get!(Value_[])) {
						res ~= fn([Value(el)]);
					}
					return Value(res);
				}
				if(args.length == 2) {
					Value x = args[0].peek!(Value_[]) is null ? Value([args[0]]) : args[0];
					Value y = args[1].peek!(Value_[]) is null ? Value([args[1]]) : args[1];
					ulong lx = x.get!(Value_[]).length;
					ulong ly = y.get!(Value_[]).length;
					Value[] res;
					for(int i; i < lx && i < ly; i++) {
						res ~= fn([x[i], y[i]]);
					}
					return Value(res);
				}
				throw new Exception("RAD: INTERNAL ERROR");
			});
		}
		throw new Exception("RAD: DOMAIN ERROR");
	}
	throw new Exception("RAD: INTERNAL ERROR");
}

Value tildeDiaeresis(Value[] arg) { //⍨: #89
	if(arg.length == 1) {
		Value fn = arg[0];
		if(fn.peek!Function !is null) {
			return standardize(delegate Value(Value[] args) {
				if(args.length == 1) {
					return fn([args[0], args[0]]);
				}
				if(args.length == 2) {
					return fn([args[1], args[0]]);
				}
				throw new Exception("RAD: INTERNAL ERROR");
			});
		}
		throw new Exception("RAD: DOMAIN ERROR");
	}
	throw new Exception("RAD: INTERNAL ERROR");
}

Value fSlashBar(Value[] arg) { //⌿: #8A
	if(arg.length == 1) {
		if(arg[0].peek!Function !is null) {
			return standardize(delegate Value(Value[] args) {
				if(args.length == 1) {
					return vectorizeMonadic(delegate Value(Value[] inner) {
						return RTLReduce(toFn(arg[0]), inner[0]);
					}, 1, args[0]);
				}
				if(args.length == 2) {
					throw new Exception("RAD: NYI ERROR");
				}
				throw new Exception("RAD: INTERNAL ERROR");
			});
		}
		throw new Exception("RAD: NYI ERROR");
	}
	throw new Exception("RAD: INTERNAL ERROR");
}

Value bSlashBar(Value[] arg) { //⍀: #8B
	if(arg.length == 1) {
		if(arg[0].peek!Function !is null) {
			return standardize(delegate Value(Value[] args) {
				if(args.length == 1) {
					return vectorizeMonadic(delegate Value(Value[] inner) {
						return RTLScan(toFn(arg[0]), inner[0]);
					}, 1, args[0]);
				}
				if(args.length == 2) {
					throw new Exception("RAD: NYI ERROR");
				}
				throw new Exception("RAD: INTERNAL ERROR");
			});
		}
		throw new Exception("RAD: NYI ERROR");
	}
	throw new Exception("RAD: INTERNAL ERROR");
}

Value lteq(Value[] arg) { //≤: #8C
	//I'm not typing the full name
	if(arg.length == 1) {
		throw new Exception("RAD: NYI ERROR");
	}
	if(arg.length == 2) {
		if(isRealValue(arg[0]) && isRealValue(arg[1])) {
			return Value(int(arg[0].get!Number.re <= arg[1].get!Number.re));
		}
		throw new Exception("RAD: DOMAIN ERROR");
	}
	throw new Exception("RAD: INTERNAL ERROR");
}

Value gteq(Value[] arg) { //≥: #8D
	//I'm not typing the full name
	if(arg.length == 1) {
		throw new Exception("RAD: NYI ERROR");
	}
	if(arg.length == 2) {
		if(isRealValue(arg[0]) && isRealValue(arg[1])) {
			return Value(int(arg[0].get!Number.re >= arg[1].get!Number.re));
		}
		throw new Exception("RAD: DOMAIN ERROR");
	}
	throw new Exception("RAD: INTERNAL ERROR");
}

Value neq(Value[] arg) { //≠: #8E
	//I'm not typing the full name
	if(arg.length == 1) {
		throw new Exception("RAD: NYI ERROR");
	}
	if(arg.length == 2) {
		return Value(int(arg[0] != arg[1]));
	}
	throw new Exception("RAD: INTERNAL ERROR");
}

Value GCD(Value[] arg) { //∨: #8F
	if(arg.length == 1) {
		throw new Exception("RAD: NYI ERROR");
	}
	if(arg.length == 2) {
		if(isRealValue(arg[0]) && isRealValue(arg[1])) {
			return Value(gcd(arg[0].get!Number.re, arg[1].get!Number.re));
		}
		throw new Exception("RAD: DOMAIN ERROR");
	}
	throw new Exception("RAD: INTERNAL ERROR");
}

Value LCM(Value[] arg) { //∧: #90
	if(arg.length == 1) {
		throw new Exception("RAD: NYI ERROR");
	}
	if(arg.length == 2) {
		if(isRealValue(arg[0]) && isRealValue(arg[1])) {
			if(arg[0] * arg[1] == Value(0)) return Value(0); //division by `0` will cause errors.
			return std.complex.abs(arg[0].get!Number * arg[1].get!Number) / GCD(arg);
		}
		throw new Exception("RAD: DOMAIN ERROR");
	}
	throw new Exception("RAD: INTERNAL ERROR");
}

Value division(Value[] arg) {//÷: #91
	if(arg.length == 1) {
		if(arg[0].peek!Number !is null) {
			return 1.0 / arg[0];
		}
		throw new Exception("RAD: DOMAIN ERROR");
	}
	if(arg.length == 2) {
		if(arg[0].peek!Number !is null && arg[0].peek!Number !is null) {
			return arg[0] / arg[1];
		}
		throw new Exception("RAD: DOMAIN ERROR");
	}
	throw new Exception("RAD: INTERNAL ERROR");
}

Value multiplication(Value[] arg) { //×: #92
	if(arg.length == 1) {
		if(arg[0].peek!Number !is null) {
			if(arg[0] == Value(0)) {
				return Value(0);
			}
			return Value(arg[0] / std.complex.abs(arg[0].get!Number));
		}
		throw new Exception("RAD: DOMAIN ERROR");
	}
	if(arg.length == 2) {
		if(arg[0].peek!Number !is null && arg[1].peek!Number !is null) {
			return arg[0] * arg[1];
		}
		throw new Exception("RAD: DOMAIN ERROR");
	}
	throw new Exception("RAD: INTERNAL ERROR");
}

Value memberOf(Value[] arg) { //∊: #93
	if(arg.length == 1) {
		if(arg[0].peek!(Value_[]) is null) { //If we have a scalar
			return Value(arg);
		}
		Value res = Value([]);
		foreach(el; arg[0].get!(Value_[])) {
			res ~= memberOf([Value(el)]);
		}
		return res;
	}
	if(arg.length == 2) {
		if(arg[1].peek!(Value_[]) is null) {
			return memberOf([arg[0], Value([arg[1]])]);
		}
		if(arg[0].peek!(Value_[]) !is null) {
			Value[] res;
			Value_[] arr = arg[1].get!(Value_[]);
			foreach(i; arg[0].get!(Value_[])) {
				res ~= Value(int(arr.contains(i)));
			}
			return Value(res);
		}
		return Value(int(arg[1].get!(Value_[]).contains(arg[0].theValue)));
	}
	throw new Exception("RAD: INTERNAL ERROR");
}

Value upArrow(Value[] arg) { //↑: #94
	if(arg.length == 1) {
		if(arg[0].peek!(Value_[]) is null) {
			return arg[0];
		}
		int len = 0;
		foreach(el; arg[0].get!(Value_[])) {
			if(el.peek!(Value_[]) is null) {
			} else {
				if(el.get!(Value_[]).length > len) {
					len = el.get!(Value_[]).length.to!int;
				}
			}
		}
		return Value(arg[0].get!(Value_[]).map(delegate Value(Value_ inner) {
			return upArrow([Value(len), Value(inner)]);
		}));
	}
	if(arg.length == 2) {
		if(arg[0].peek!(Value_[]) is null) {
			return upArrow([Value([arg[0]]), arg[1]]);
		}
		if(arg[1].peek!(Value_[]) is null) {
			return upArrow([arg[0], Value([arg[1]])]);
		}
		if(arg[0].get!(Value_[]).length == 0) {
			return arg[1];
		}
		if(arg[0].get!(Value_[]).length == 1) {
			int s = arg[0][0].get!Number.re.to!int;
			if(0 <= s && s <= arg[1].get!(Value_[]).length) {
				return Value(arg[1].get!(Value_[])[0 .. s]);
			}
			if(0 <= -s && -s <= arg[1].get!(Value_[]).length) {
				return Value(arg[1].get!(Value_[])[$+s .. $]);
			}
			if(s > 0 && arg[1].get!(Value_[]).length < s) {
				Value S = repeat(prototype(arg[1]), s - arg[1].get!(Value_[]).length.to!int);
				return Value(arg[1].get!(Value_[]) ~ S.get!(Value_[]));
			}
			if(s < 0 && arg[1].get!(Value_[]).length < -s) {
				Value S = repeat(prototype(arg[1]), (-s) - arg[1].get!(Value_[]).length.to!int);
				return Value(S.get!(Value_[]) ~ arg[1].get!(Value_[]));
			}
			throw new Exception("RAD: INTERNAL ERROR");
		}
		return Value(upArrow([arg[0][0], arg[1]]).get!(Value_[]).map(delegate Value(Value_ inner) {
			return upArrow([Value(arg[0].get!(Value_[])[1 .. $]), Value(inner)]);
		}));
	}
	throw new Exception("RAD: INTERNAL ERROR");
}

Value downArrow(Value[] arg) { //↓: #95
	if(arg.length == 1) {
		return downArrow([Value(1), arg[0]]);
	}
	if(arg.length == 2) {
		if(arg[0].peek!(Value_[]) is null) {
			return downArrow([Value([arg[0]]), arg[1]]);
		}
		if(arg[1].peek!(Value_[]) is null) {
			return downArrow([arg[0], Value([arg[1]])]);
		}
		if(arg[0].get!(Value_[]).length == 0) {
			return arg[1];
		}
		if(arg[0].get!(Value_[]).length == 1) {
			int s = arg[0][0].get!Number.re.to!int;
			if(0 <= s && s <= arg[1].get!(Value_[]).length) {
				return Value(arg[1].get!(Value_[])[s .. $]);
			}
			if(0 <= -s && -s <= arg[1].get!(Value_[]).length) {
				return Value(arg[1].get!(Value_[])[0 .. $+s]);
			}
			if(s > 0 && arg[1].get!(Value_[]).length < s) {
				return Value([]);
			}
			if(s < 0 && arg[1].get!(Value_[]).length < -s) {
				return Value([]);
			}
			throw new Exception("RAD: INTERNAL ERROR");
		}
		return Value(downArrow([arg[0][0], arg[1]]).get!(Value_[]).map(delegate Value(Value_ inner) {
			return downArrow([Value(arg[0].get!(Value_[])[1 .. $]), Value(inner)]);
		}));
	}
	throw new Exception("RAD: INTERNAL ERROR");
}

Value circle(Value[] arg_) { //○: #96
	if(arg_.length == 1) {
		return PI * arg_[0];
	}
	if(arg_.length == 2) {
		if(isIntValue(arg_[0]) && arg_[1].peek!Number !is null) {
			int x = arg_[0].get!Number.re.to!int;
			double y = arg_[1].get!Number.re;
			Number z = arg_[1].get!Number;
			switch(x) {
				case 0:
					return Value((1-z^^2)^^0.5);
				case -1:
					return Value(asin(y));
				case 1:
					return Value(sin(z));
				case -2:
					return Value(acos(y));
				case 2:
					return Value(cos(z));
				case -3:
					return Value(atan(y));
				case 3:
					return Value(tan(y));
				case -4:
					return Value((z^^2-1)^^0.5);
				case 4:
					return Value(int(.5 != 1+z^^2));
				case -5:
					return Value(asinh(y));
				case 5:
					return Value(sinh(y));
				case -6:
					return Value(acosh(y));
				case 6:
					return Value(cosh(y));
				case -7:
					return Value(atanh(y));
				case 7:
					return Value(tanh(y));
				case -8:
					return -circle([Value(8), arg_[1]]);
				case 8:
					return Value((-(1+z^^2))^^0.5);
				case -9:
					return Value(z);
				case 9:
					return Value(z.re);
				case -10:
					return Value(conj(z));
				case 10:
					return Value(std.complex.abs(z));
				case -11:
					return Value(z*Number(0,1));
				case 11:
					return Value(z.im);
				case -12:
					return Value(asterisk([Value(1)])^^(z*Number(0,1)));
				case 12:
					return Value(arg(z));
				default:
					throw new Exception("RAD: DOMAIN ERROR");
			}
		}
		throw new Exception("RAD: DOMAIN ERROR");
	}
	throw new Exception("RAD: INTERNAL ERROR");
}

Value lceil(Value[] arg) { //⌈: #97
	if(arg.length == 1) {
		if(arg[0].peek!Number !is null) {
			return Value(Number(ceil(arg[0].get!Number.re), ceil(arg[0].get!Number.im)));
		}
		throw new Exception("RAD: DOMAIN ERROR");
	}
	if(arg.length == 2) {
		if(arg[0].peek!Number !is null && arg[1].peek!Number !is null) {
			return Value(Number(fmax(arg[0].get!Number.re, arg[1].get!Number.re), fmax(arg[0].get!Number.im, arg[1].get!Number.im)));
		}
		throw new Exception("RAD: DOMAIN ERROR");
	}
	throw new Exception("RAD: INTERNAL ERROR");
}

Value lfloor(Value[] arg) { //⌊: #98
	if(arg.length == 1) {
		if(arg[0].peek!Number !is null) {
			return Value(Number(floor(arg[0].get!Number.re), floor(arg[0].get!Number.im)));
		}
		throw new Exception("RAD: DOMAIN ERROR");
	}
	if(arg.length == 2) {
		if(arg[0].peek!Number !is null && arg[1].peek!Number !is null) {
			return Value(Number(fmin(arg[0].get!Number.re, arg[1].get!Number.re), fmin(arg[0].get!Number.im, arg[1].get!Number.im)));
		}
		throw new Exception("RAD: DOMAIN ERROR");
	}
	throw new Exception("RAD: INTERNAL ERROR");
}

Value jot(Value[] arg0) { //∘: #9A
	if(arg0.length == 1) {
		return Value(delegate Value(Value[] arg1) {
			if(arg1.length == 1) {
				return standardize(delegate Value(Value[] arg2) {
					Value g = arg0[0];
					Value f = arg1[0];
					if(arg2.length == 1) {
						if(f.peek!Function !is null && g.peek!Function !is null) {
							return f([g(arg2)]);
						}
						if(f.peek!Function is null && g.peek!Function !is null) {
							return g([f, arg2[0]]);
						}
						if(f.peek!Function !is null && g.peek!Function is null) {
							return f([arg2[0], g]);
						}
						throw new Exception("RAD: DOMAIN ERROR");
					}
					if(arg2.length == 2) {
						if(f.peek!Function !is null && g.peek!Function !is null) {
							return f([arg2[0], g([arg2[1]])]);
						}
						throw new Exception("RAD: DOMAIN ERROR");
					}
					throw new Exception("RAD: INTERNAL ERROR");
				});
			}
			throw new Exception("RAD: INTERNAL ERROR");
		});
	}
	throw new Exception("RAD: INTERNAL ERROR");
}

Value ssubset(Value[] arg) { //⊂: #9B
	if(arg.length == 1) {
		return Value(arg);
	}
	if(arg.length == 2) {
		throw new Exception("RAD: NYI ERROR");
	}
	throw new Exception("RAD: INTERNAL ERROR");
}

Value ssupset(Value[] arg) { //⊃: #9C
	if(arg.length == 1) {
		Value res = arg[0];
		while(res.peek!(Value_[]) !is null && res.get!(Value_[]).length > 0) {
			res = res[0];
		}
		if(res.peek!(Value_[]) !is null) {
			return Value(0); //FIXME: prototypes.
		}
		return res;
	}
	if(arg.length == 2) {
		if(arg[0].peek!(Value_[]) is null) {
			return ssupset([Value([arg[0]]), arg[1]]);
		}
		Value[] arr = arg[0].get!(Value_[]).map(delegate Value(Value_ inner) {
			return Value(inner);
		});
		return indexBrackets([arg[0], arr.reduce!((a, b) => Value(new Semicolon(a, b)))]);
	}
	throw new Exception("RAD: INTERNAL ERROR");
}

Value intersect(Value[] arg) { //∩: #9D
	if(arg.length == 1) {
		if(arg[0].peek!(Value_[]) is null) {
			return Value([]);
		}
		Value[] res;
		Value[] tmp;
		foreach(i; arg[0].get!(Value_[])) {
			if(tmp.contains(Value(i))) {
				res ~= Value(i);
			} else {
				tmp ~= Value(i);
			}
		}
		return Value(res);
	}
	if(arg.length == 2) {
		if(arg[1].peek!(Value_[]) is null) {
			return intersect([arg[0], Value([arg[1]])]);
		}
		if(arg[0].peek!(Value_[]) is null) {
			return intersect([Value([arg[0]]), arg[1]]);
		}
		Value[] res;
		foreach(i; arg[0].get!(Value_[])) {
			if(arg[1].get!(Value_[])().contains(i)) {
				res ~= Value(i);
			}
		}
		return Value(res);
	}
	throw new Exception("RAD: INTERNAL ERROR");
}

Value theUnion(Value[] arg) { //∪: #9E (named `theUnion` to not be a keyword)
	if(arg.length == 1) {
		if(arg[0].peek!(Value_[]) is null) {
			return Value(arg);
		}
		Value[] res;
		foreach(i; arg[0].get!(Value_[])) {
			if(!res.contains(Value(i))) {
				res ~= Value(i);
			}
		}
		return Value(res);
	}
	if(arg.length == 2) {
		return comma([arg[0], tilde([arg[1], arg[0]])]);
	}
	throw new Exception("RAD: INTERNAL ERROR");
}

Value nor(Value[] arg) { //⍱: #A1
	if(arg.length == 1) {
		throw new Exception("RAD: NYI ERROR");
	}
	if(arg.length == 2) {
		return tilde([GCD(arg)]);
	}
	throw new Exception("RAD: INTERNAL ERROR");
}

Value nand(Value[] arg) { //⍲: #A2
	if(arg.length == 1) {
		throw new Exception("RAD: NYI ERROR");
	}
	if(arg.length == 2) {
		return tilde([LCM(arg)]);
	}
	throw new Exception("RAD: INTERNAL ERROR");
}

Value gradeDown(Value[] arg) { //⍒: #A3
	if(arg.length == 1) {
		throw new Exception("RAD: NYI ERROR");
	}
	if(arg.length == 2) {
		throw new Exception("RAD: NYI ERROR");
	}
	throw new Exception("RAD: INTERNAL ERROR");
}

Value gradeUp(Value[] arg) { //⍋: #A4
	if(arg.length == 1) {
		throw new Exception("RAD: NYI ERROR");
	}
	if(arg.length == 2) {
		throw new Exception("RAD: NYI ERROR");
	}
	throw new Exception("RAD: INTERNAL ERROR");
}

Value transpose(Value[] arg) { //⍉: #A5
	if(arg.length == 1) {
		return fSlash([Value(diaeresis([Value(&comma)]))])(arg);
	}
	if(arg.length == 2) {
		throw new Exception("RAD: NYI ERROR");
	}
	throw new Exception("RAD: INTERNAL ERROR");
}

Value rphi(Value[] arg) { //⌽: #A6
	if(arg.length == 1) {
		if(arg[0].peek!(Value_[]) is null) {
			return arg[0];
		}
		Value[] res;
		foreach_reverse(i; arg[0].get!(Value_[])) {
			res ~= Value(i);
		}
		return Value(res);
	}
	if(arg.length == 2) {
		throw new Exception("RAD: NYI ERROR");
	}
	throw new Exception("RAD: INTERNAL ERROR");
}

Value rtheta(Value[] arg) { //⊖: #A7
	if(arg.length == 1) {
		return rphi(arg);
	}
	if(arg.length == 2) {
		throw new Exception("RAD: NYI ERROR");
	}
	throw new Exception("RAD: INTERNAL ERROR");
}

Value circleStar(Value[] arg) { //⍟: #A8
	if(arg.length == 1) {
		if(isRealValue(arg[0])) {
			return Value(log(arg[0].get!Number.re));
		}
		throw new Exception("RAD: DOMAIN ERROR");
	}
	if(arg.length == 2) {
		if(isRealValue(arg[0]) && isRealValue(arg[1])) {
			return Value(log(arg[0].get!Number.re) / log(arg[1].get!Number.re));
		}
		throw new Exception("RAD: DOMAIN ERROR");
	}
	throw new Exception("RAD: INTERNAL ERROR");
}

Value commaBar(Value[] arg) { //⍪: #AD
	if(arg.length == 1) {
		return diaeresis([Value(&memberOf)])(arg);
	}
	if(arg.length == 2) {
		return comma([arg[0], Value([arg[1]])]);
	}
	throw new Exception("RAD: INTERNAL ERROR");
}

Value identical(Value[] arg) { //≡: #AE
	if(arg.length == 1) {
		return Value(depth(arg[0]));
	}
	if(arg.length == 2) {
		return Value(int(arg[0] == arg[1]));
	}
	throw new Exception("RAD: INTERNAL ERROR");
}

Value notIdentical(Value[] arg) { //≢: #AF
	if(arg.length == 1) {
		if(arg[0].peek!(Value_[]) is null) {
			return Value(1);
		}
		return Value(arg[0].get!(Value_[]).length);
	}
	if(arg.length == 2) {
		return Value(int(arg[0] != arg[1]));
	}
	throw new Exception("RAD: INTERNAL ERROR");
}

Value assign(Value[] arg) { //←: #B1
	if(arg.length == 1) {
		throw new Exception("RAD: NYI ERROR");
	}
	if(arg.length == 2) {
		dstring tmp;
		foreach(c; arg[0].get!(Value_[]))
			tmp ~= c.get!dchar;
		setVariable(tmp, arg[1]);
		return Value(arg[1]);
	}
	throw new Exception("RAD: INTERNAL ERROR");
}

Value quadLeftArrow(Value[] arg) { //⍇: #BC
	if(arg.length == 1) {
		writeln(repr(arg[0], true));
		return arg[0];
	}
	if(arg.length == 2) {
		throw new Exception("RAD: NYI ERROR");
	}
	throw new Exception("RAD: INTERNAL ERROR");
}

Value jotDiaeresis(Value[] arg0) { //⍤: #C0; arg0: depths
	if(arg0.length == 1) {
		if(arg0[0].peek!(Value_[]) !is null && arg0[0][0].peek!Number !is null && arg0[0][1].peek!Number !is null && arg0[0][2].peek!Number !is null) {
			return Value(delegate Value(Value[] arg1) { //arg1: function
				if(arg1[0].peek!Function !is null) {
					return standardize(vectorize(delegate Value(Value[] args) { //Convert `Value_ delegate(Value_[])` to `Value delegate(Value[])`
						return Value(arg1[0].get!Function()(args.map(delegate Value_(Value a) => a.theValue)));
					}, arg0[0][0].get!Number.re.to!int, arg0[0][1].get!Number.re.to!int, arg0[0][2].get!Number.re.to!int));
				}
				throw new Exception("RAD: DOMAIN ERROR");
			});
		}
		throw new Exception("RAD: DOMAIN ERROR");
	}
	throw new Exception("RAD: INTERNAL ERROR");
}

Value boxFSlash(Value[] arg) { //⍁: #C1
	if(arg.length == 1) {
		if(arg[0].peek!Function !is null) {
			return standardize(delegate Value(Value[] args) {
				if(args.length == 1) {
					return LTRReduce(toFn(arg[0]), args[0]);
				}
				if(args.length == 2) {
					throw new Exception("RAD: NYI ERROR");
				}
				throw new Exception("RAD: INTERNAL ERROR");
			});
		}
		throw new Exception("RAD: NYI ERROR");
	}
	throw new Exception("RAD: INTERNAL ERROR");
}

Value boxBSlash(Value[] arg) { //⍂: #C2
	if(arg.length == 1) {
		if(arg[0].peek!Function !is null) {
			return standardize(delegate Value(Value[] args) {
				if(args.length == 1) {
					return LTRScan(toFn(arg[0]), args[0]);
				}
				if(args.length == 2) {
					throw new Exception("RAD: NYI ERROR");
				}
				throw new Exception("RAD: INTERNAL ERROR");
			});
		}
		throw new Exception("RAD: NYI ERROR");
	}
	throw new Exception("RAD: INTERNAL ERROR");
}


Value interrobang(Value[] arg) { //‽: #E9
	if(arg.length == 1) {
		return standardize(delegate Value(Value[] args) {
			if(args.length == 1) {
				return arg[0](args);
			}
			if(args.length == 2) {
				return arg[0](args);
			}
			throw new Exception("RAD: INTERNAL ERROR");
		});
	}
	throw new Exception("RAD: INTERNAL ERROR");
}

LazyCall atom(dstring arg) {
	Value blackHole(Value[] args) { //Just a thing that lets me test representation
		return Value(&blackHole);
	}
	//Apparently, I can't have too many `case`s in a `switch` statement on a `dstring` in GDC...
	if(arg == "α"d)
		return lowercaseAlpha();
	if(arg == "ε"d)
		return lambdify(standardize(&lowercaseEpsilon));
	if(arg == "ι"d)
		return lambdify(standardize(vectorize(&lowercaseIota, 1, -1, -1)));
	if(arg == "μ"d)
		return lambdify(standardize(vectorize(&lowercaseMu, 1, -1, -1)));
	if(arg == "π"d)
		return lambdify(Value(lowercasePi));
	if(arg == "ρ"d)
		return lambdify(standardize(&lowercaseRho));
	if(arg == "ω"d)
		return lowercaseOmega();
	if(arg == "!"d)
		return lambdify(standardize(vectorize(&exclamationMark, 0, 0, 0)));
	if(arg == "\""d || arg == "⎕UCS"d)
		return lambdify(standardize(vectorize(&quotationMark, 0, 0, 0)));
	if(arg == "&"d)
		return lambdify(Value(&ampersand));
	if(arg == "*"d)
		return lambdify(standardize(vectorize(&asterisk, 0, 0, 0)));
	if(arg == "+"d)
		return lambdify(standardize(vectorize(&plusSign, 0, 0, 0)));
	if(arg == ","d)
		return lambdify(standardize(&comma));
	if(arg == "-"d)
		return lambdify(standardize(vectorize(&hyphenMinus, 0, 0, 0)));
	if(arg == "/"d)
		return lambdify(Value(&fSlash));
	if(arg == ";"d)
		return lambdify(Value(&semicolon));
	if(arg == ";_"d)
		return lambdify(Value(delegate Value(Value[] args) {
			Number NAN = Number(Complex!double(double.nan, double.nan));
			return Value(new Semicolon(Value(NAN), args[0]));
		}));
	if(arg == "_;"d)
		return lambdify(Value(delegate Value(Value[] args) {
			Number NAN = Number(Complex!double(double.nan, double.nan));
			return Value(new Semicolon(args[0], Value(NAN)));
		}));
	if(arg == "<"d)
		return lambdify(standardize(vectorize(&lessThan, 1, 0, 0)));
	if(arg == "="d)
		return lambdify(standardize(vectorize(&equalTo, 0, 0, 0)));
	if(arg == ">"d)
		return lambdify(standardize(vectorize(&greaterThan, 1, 0, 0)));
	if(arg == "?"d)
		return lambdify(standardize(vectorize(&questionMark, 0, 0, 0)));
	if(arg == "[]"d)
		return lambdify(Value(&indexBrackets));
	if(arg == "\\"d)
		return lambdify(Value(&bSlash));
	if(arg == "|"d)
		return lambdify(standardize(vectorize(&verticalLine, 0, 0, 0)));
	if(arg == "~"d)
		return lambdify(standardize(vectorize(&tilde, 0, -1, -1)));
	if(arg == "⍬"d)
		return lambdify(Value(zilde));
	if(arg == "⊢"d)
		return lambdify(standardize(&rightTack));
	if(arg == "⊣"d)
		return lambdify(standardize(&leftTack));
	if(arg == "⌷"d)
		return lambdify(standardize(vectorize(&squishQuad, -1, 1, -1)));
	if(arg == "¨"d)
		return lambdify(Value(&diaeresis));
	if(arg == "⍨"d)
		return lambdify(Value(&tildeDiaeresis));
	if(arg == "⌿"d)
		return lambdify(Value(&fSlashBar));
	if(arg == "⍀"d)
		return lambdify(Value(&bSlashBar));
	if(arg == "≤"d)
		return lambdify(standardize(vectorize(&lteq, 0, 0, 0)));
	if(arg == "≥"d)
		return lambdify(standardize(vectorize(&gteq, 0, 0, 0)));
	if(arg == "≠"d)
		return lambdify(standardize(vectorize(&neq, 0, 0, 0)));
	if(arg == "∨"d)
		return lambdify(standardize(vectorize(&GCD, 0, 0, 0)));
	if(arg == "∧"d)
		return lambdify(standardize(vectorize(&LCM, 0, 0, 0)));
	if(arg == "÷"d)
		return lambdify(standardize(vectorize(&division, 0, 0, 0)));
	if(arg == "×"d)
		return lambdify(standardize(vectorize(&multiplication, 0, 0, 0)));
	if(arg == "∊"d)
		return lambdify(standardize(&memberOf));
	if(arg == "↑"d)
		return lambdify(standardize(vectorize(&upArrow, 2, -1, -1)));
	if(arg == "↓"d)
		return lambdify(standardize(&downArrow));
	if(arg == "○"d)
		return lambdify(standardize(vectorize(&circle, 0, 0, 0)));
	if(arg == "⌈"d)
		return lambdify(standardize(vectorize(&lceil, 0, 0, 0)));
	if(arg == "⌊"d)
		return lambdify(standardize(vectorize(&lfloor, 0, 0, 0)));
	if(arg == "∘"d)
		return lambdify(Value(&jot));
	if(arg == "⊂"d)
		return lambdify(standardize(&ssubset));
	if(arg == "⊃"d)
		return lambdify(standardize(&ssupset));
	if(arg == "∩"d)
		return lambdify(standardize(&intersect));
	if(arg == "∪"d)
		return lambdify(standardize(&theUnion));
	if(arg == "⍱"d)
		return lambdify(standardize(vectorize(&nor, 0, 0, 0)));
	if(arg == "⍲"d)
		return lambdify(standardize(vectorize(&nand, 0, 0, 0)));
	if(arg == "⍒"d)
		return lambdify(standardize(&gradeDown));
	if(arg == "⍋"d)
		return lambdify(standardize(&gradeUp));
	if(arg == "⍉"d)
		return lambdify(standardize(&transpose));
	if(arg == "⌽"d)
		return lambdify(standardize(&rphi));
	if(arg == "⊖"d)
		return lambdify(standardize(vectorize(&rtheta, 1, -1, 1)));
	if(arg == "⍟"d)
		return lambdify(standardize(vectorize(&circleStar, 0, 0, 0)));
	if(arg == "⍪"d)
		return lambdify(standardize(vectorize(&commaBar, -1, 1, 1)));
	if(arg == "≡"d)
		return lambdify(standardize(&identical));
	if(arg == "≢"d)
		return lambdify(standardize(&notIdentical));
	if(arg == "←"d)
		return lambdify(Value(&assign));
	if(arg == "⍇"d)
		return lambdify(standardize(&quadLeftArrow));
	if(arg == "⍤"d)
		return lambdify(Value(&jotDiaeresis));
	if(arg == "⍁"d)
		return lambdify(Value(&boxFSlash));
	if(arg == "⍂"d)
		return lambdify(Value(&boxBSlash));
	if(arg == "‽"d)
		return lambdify(Value(&interrobang));
	return new LazyCall(toDelegate(&getVariable), [Value(arg)], arg); //Variables
}
