module RAD_Comment;

import std.regex, std.functional;

dstring Comment() {
	return "(?:⍝.*)"d;
}
alias CommentRE = memoize!(() => regex("^"d ~ Comment() ~ "$"d));

bool isComment(dstring arg) {
	auto c = matchFirst(arg, CommentRE);
	return !!c; //check if the match is the entire string
}


