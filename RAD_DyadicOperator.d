module RAD_DyadicOperator;

import std.regex, std.functional;
import RAD_Value, RAD_LazyCall, RAD_Atoms;
import RAD_Alias, RAD_VariableName;

dstring DyadicOperatorAtom() {
	return "(?:[@∘⍣⌺⍢⍤⊙⍩])"d; //1-char
}
alias DyadicOperatorAtomRE = memoize!(() => regex("^"d ~ DyadicOperatorAtom() ~ "$"d));

bool isDyadicOperatorAtom(dstring arg) {
	auto c = matchFirst(arg, DyadicOperatorAtomRE);
	return !!c; //check if the match is the entire string	
}
LazyCall parseDyadicOperatorAtom(dstring arg) {
	return atom(dealias(arg));
}

dstring DyadicOperatorVariable() {
	return "(?:⍙"d ~ VariableTail() ~ ")"d;
}
alias DyadicOperatorVariableRE = memoize!(() => regex("^"d ~ DyadicOperatorVariable() ~ "$"d));

bool isDyadicOperatorVariable(dstring arg) {
	auto c = matchFirst(arg, DyadicOperatorVariableRE);
	return !!c; //check if the match is the entire string
}
LazyCall parseDyadicOperatorVariable(dstring arg) {
	return atom(arg);
}
