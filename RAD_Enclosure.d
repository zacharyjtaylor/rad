module RAD_Enclosure;

import std.stdio;
import RAD_Value, RAD_LazyCall, RAD_Tokenize;
import RAD_Evaluate, RAD_Variable, RAD_AtomUtils;


Value functionEnclosure(Segment[] code) { //{}
	return standardize(delegate Value(Value[] args) {
		newScope(args);
		setVariable("∇"d, functionEnclosure(code)); //I think this works, but I have no way to check without properly handling conditionals.
		Value ev = activate(evaluate(code).value);
		destroyScope();
		return ev;
	});
}
