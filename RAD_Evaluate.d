module RAD_Evaluate;

import std.variant, std.regex, std.stdio, std.complex, std.typecons;
import std.array : split;
import std.functional : toDelegate;
import RAD_Value, RAD_LazyCall, RAD_Tokenize;
import RAD_utils, RAD_Atoms, RAD_Enclosure;

dstring[] groups(Segment[] arg) { //returns the array of Segment groups
	return arg.map(delegate dstring(Segment a) => a.group);
}

Segment evaluate(Segment[] res) { //Argument name `res` is due to the Python version. 
	ulong res_i;
	ulong res_j;
	dstring res_P;
	LazyCall res_V;
	Segment res_S;
	bool stop_flag;	
	
	Value f(Segment arg) { //Helper function for a lot of things
		return activate(arg.value);
	}

	dstring toRep(Segment arg) {
		return arg.rep;
	}

	if(res.length == 1) { //If we have just one Segment
		return res[0];
	}

	/+ Step 1: simplify enclosures +/
	for(ulong j = res.length - 1; j < res.length && j >= 0; j--) {
		if(stop_flag) break;
		for(ulong i = 0; i < j; i++) {
			if(stop_flag) break;
			Segment[] S = res[i .. j+1]; //Current section
			dstring[] I = groups(res[i+1 .. j]);
			if(((S[0].group ~ S[$ - 1].group) == "()"d ? !I.contains("("d) && !I.contains(")"d) : true) && !I.contains("{"d) && !I.contains("}"d) && !I.contains("⦃"d) && !I.contains("⦄"d)) {
				dstring P = S[0].group ~ S[$ - 1].group;
				if(P == "()"d || P == "{}"d || P == "⦃}"d || P == "{⦄"d || P == "⦃⦄"d) {
					res_P = P;
					res_i = i;
					res_j = j;
					stop_flag = true;
				}
			}
		}
	}
	if(stop_flag) {
		Segment S, T;
		Value U;
		switch(res_P) {
			/*case "()"d:
			T = evaluate(res[res_i+1 .. res_j]); //Evaluated tokens
			S = Segment(T.group, T.value, "("d ~ T.rep ~ ")"d); //Create segment
			res.collapse(res_i, res_j+1, [S]); //Collapse
			return evaluate(res);
			break;*/
	
			case "{}"d:
			U = functionEnclosure(res[res_i+1 .. res_j]);
			S = Segment("F"d, U, "{...}"d); //Don't know how to fix that.
			res.collapse(res_i, res_j+1, [S]);
			return evaluate(res);
			break;
	
			case "⦃}"d:
			T = evaluate(res[res_i+1 .. res_j]);
			S = Segment("M"d, T.value, "⦃"d ~ T.rep ~ "}"d);
			res.collapse(res_i, res_j+1, [S]);
			return evaluate(res);
			break;
	
			case "{⦄"d:
			T = evaluate(res[res_i+1 .. res_j]);
			S = Segment("D"d, T.value, "{"d ~ T.rep ~ "⦄"d);
			res.collapse(res_i, res_j+1, [S]);
			return evaluate(res);
			break;
	
			case "⦃⦄"d:
			T = evaluate(res[res_i+1 .. res_j]);
			S = Segment("D"d, T.value, "⦃"d ~ T.rep ~ "⦄"d);
			res.collapse(res_i, res_j+1, [S]);
			return evaluate(res);
			break;
	
			default:
			break;
		}
	}

	/+ Step 1.5: multiple line statements (was step 7) +/
	if(groups(res).contains("⋄"d)) {
		//Segment[] T = res.split!(x => x.group == "⋄"d).map((&evaluate).toDelegate);
		Tuple!(Segment, Value)[] result = res.split!(x => x.group == "⋄"d).map(delegate Tuple!(Segment, Value) (Segment[] arg) {
			Segment ev = evaluate(arg);
			Value va = activate(ev.value);
			return tuple(ev, va);
		});
		return Segment(result[0][0].group, result[$ - 1][1], ""d.join(res.map(&toRep)));
	}

	/+ Step 1.75: parentheses (was part of step 1)+/
	if(res_P == "()"d) {
		Segment[] T = res[res_i+1 .. res_j]; //Evaluated tokens
		Segment S = Segment(groupify(T), new LazyCall(delegate Value(Value[] args) {
			return activate(evaluate(T).value);
		}, [], "_"d), "("d ~ ""d.join(T.map(delegate dstring(Segment x) {
			return x.rep;
		})) ~ ")"d); //Create segment
		res.collapse(res_i, res_j+1, [S]); //Collapse
		return evaluate(res);
	}

	/+ Step 2: collapse vectors +/
	for(ulong j = res.length - 1; j < res.length && j >= 0; j--) {
		if(stop_flag) break;
		for(ulong i = 0; i < j; i++) {
			if(stop_flag) break;
			Segment[] S = res[i .. j+1];
			dstring[] I = groups(S);
			if(S.length >= 2 && I.isAll("N"d)) {
				stop_flag = true;
				res_i = i;
				res_j = j;
			}
		}
	}
	if(stop_flag) {
		Segment[] T = res[res_i .. res_j+1];
		Value V = Value(T.mapRTL(&f));
		Segment S = Segment("N"d, V, " "d.join(T.map(&toRep)));
		res.collapse(res_i, res_j+1, [S]);
		return evaluate(res);
	}

	/+ Step 3: Dyadic Operator => Monadic Operator +/
	for(ulong j = res.length - 1; j > 0; j--) {
		dstring[] T = groups(res[j-1 .. j+1]);
		if(T[0] == "D"d && (T[1] == "N"d || T[1] == "F"d)) {
			stop_flag = true;
			res_j = j;
		}
	}
	if(stop_flag) {
		Segment[] T = res[res_j-1 .. res_j+1];
		Value V = activate(T[0].value([activate(T[1].value)])); //FIXME: this might trigger a side-effect too early.
		Segment S = Segment("M"d, V, T[0].rep ~ " "d ~ T[1].rep);
		res.collapse(res_j-1, res_j+1, [S]);
		return evaluate(res);
	}

	/+ Step 4: Monadic Operator => Function +/
	for(ulong i = res.length - 2; i < res.length && i >= 0; i--) {
		dstring[] T = groups(res[i .. i+2]);
		if(T[1] == "M"d && (T[0] == "N"d || T[0] == "F"d)) {
			stop_flag = true;
			res_i = i;
		}
	}
	if(stop_flag) {
		Segment[] T = res[res_i .. res_i+2];
		Value V = activate(T[1].value([activate(T[0].value)])); //FIXME: this might trigger a side-effect too early
		Segment S = Segment("F"d, V, T[0].rep ~ " " ~ T[1].rep);
		res.collapse(res_i, res_i+2, [S]);
		return evaluate(res);
	}

	/+ Step 5: Function calls, indexing, assignment, trains +/
	for(ulong j = res.length - 1; j < res.length && j >= 0; j--) {
		if(stop_flag) break;
		for(ulong i = 0; i < j; i++) {
			if(stop_flag) break;

			res_i = i;
			res_j = j;
			Segment[] S = res[i .. j+1];
			dstring T = ""d.join(groups(S));
			stop_flag = true;

			switch(T) {
				case "N[N]"d: //Indexing
				Value S2 = activate(S[2].value);
				Value S0 = activate(S[0].value);
				res_V = atom("[]"d)([S0, S2]);
				res_S = Segment("N"d, res_V, S[0].rep ~ "["d ~ S[2].rep ~ "]"d);
				break;

				case "NF←N"d: //Modified assignment
				Value S3 = activate(S[3].value);
				Value S1 = activate(S[1].value);
				Value S0 = activate(S[0].value);
				res_V = atom("←"d)([S0, S1, S3]);
				res_S = Segment("N"d, res_V, S[0].rep ~ " "d ~ S[1].rep ~ "←"d ~ S[3].rep);
				break;

				case "FFF"d: //`f g h` fork
				Value S2 = activate(S[2].value);
				Value S0 = activate(S[0].value);
				res_V = S[1].value([S0, S2]); //FIXME This calls a function with functions, BE CAREFUL.
				res_S = Segment("F"d, res_V, S[0].rep ~ " "d ~ S[1].rep ~ " "d ~ S[2].rep);
				break;

				case "NFF"d: //`A g h` fork
				Value S2 = activate(S[2].value);
				Value S0 = activate(S[0].value);
				res_V = S[1].value([S0, S2]);
				res_S = Segment("F"d, res_V, S[0].rep ~ " "d ~ S[1].rep ~ " "d ~ S[2].rep);
				break;

				case "NFN"d: //Dyadic function application
				Value S2 = activate(S[2].value);
				Value S0 = activate(S[0].value);
				res_V = S[1].value([S0, S2]);
				res_S = Segment("N"d, res_V, S[0].rep ~ " "d ~ S[1].rep ~ " "d ~ S[2].rep);
				break;

				case "N[]"d: //Identity-indexing
				Value S0 = activate(S[0].value);
				res_V = atom("[]")([S0]);
				res_S = Segment("N"d, res_V, S[0].rep ~ "[]"d);
				break;

				case "N;N"d: //Multiple-layered indexing notation
				Value S2 = activate(S[2].value);
				Value S0 = activate(S[0].value);
				res_V = atom(";"d)([S0, S2]);
				res_S = Segment("N"d, res_V, S[0].rep ~ ";"d ~ S[2].rep);
				break;

				case "N←N"d: //Assignment
				case "F←F"d:
				case "M←M"d:
				case "D←D"d:
				Value S2 = activate(S[2].value);
				res_V = atom("←"d)([Value(S[0].rep), S2]);
				res_S = Segment(T[0 .. 1], res_V, S[0].rep ~ "←"d ~ S[2].rep);
				break;

				case "FF"d: //Atop (`f g`)
				Value S1 = activate(S[1].value);
				res_V = S[0].value([S1]);
				res_S = Segment("F"d, res_V, S[0].rep ~ " "d ~ S[1].rep);
				break;

				case "FN"d: //Monadic function application
				Value S1 = activate(S[1].value);
				res_V = S[0].value([S1]);
				res_S = Segment("N"d, res_V, S[0].rep ~ " "d ~ S[1].rep);
				break;

				case ";N"d: //More `;` indexing
				Value S1 = activate(S[1].value);
				res_V = atom(";_"d)([S1]);
				res_S = Segment("N"d, res_V, ";"d ~ S[1].rep);
				break;

				case "N;"d:
				Value S0 = activate(S[0].value);
				res_V = atom("_;"d)([S0]);
				res_S = Segment("N"d, res_V, S[0].rep ~ ";"d);
				break;

				case ";;"d: //A hack to get semicolon parsing to work
				Value NAN = Value(Complex!double(double.nan, double.nan));
				res_S = Segment("N"d, 
					Value(new Semicolon(
						Value(new Semicolon(
							NAN, NAN
						)),
					NAN)),
					";;"d
				);
				break;

				case "N;;"d: //I have no idea how this works, but it does.
				Value NAN = Value(Complex!double(double.nan, double.nan));
				res_S = Segment("N"d, 
					Value(new Semicolon(
						Value(new Semicolon(
							activate(S[0].value), NAN
						)),
					NAN)),
					S[0].rep ~ ";;"d
				);
				break;

				case ";"d: //This is never reached, for some reason
				res_V = atom(";")([]);
				res_S = Segment("N"d, res_V, ";"d);
				break;

				default:
				stop_flag = false;
				break;
			}
		}
	}
	if(stop_flag) {
		res.collapse(res_i, res_j+1, [res_S]);
		return evaluate(res);
	}
	
	/+ Step 6: conditional branching/ifs +/
	for(ulong j = res.length - 1; j < res.length && j >= 0; j--) {
		for(ulong i = 0; i < j; i++) {
			Segment[] S = res[i .. j+1];
			dstring T = ""d.join(groups(S));
			if(T == "N:N"d) {
				res_i = i;
				res_j = j;
				Value S0 = activate(S[0].value);
				if(S0.peek!Number !is null && S0.get!Number != Number(0, 0)) {
					res_V = S[2].value;
				} else {
					res_V = S[0].value;
				}
				res_S = Segment("N"d, res_V, S[0].rep ~ ":"d ~ S[2].rep);
				stop_flag = true;
			}
		}
	}
	if(stop_flag) {
		res.collapse(res_i, res_j+1, [res_S]);
		return evaluate(res);
	}
	/+ Step 7 has moved +/
	writeln(res);
	throw new Exception("RAD: EVALUATION ERROR");
}
