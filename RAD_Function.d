module RAD_Function;

import std.regex, std.functional;
import RAD_Value, RAD_LazyCall, RAD_Atoms;
import RAD_Alias, RAD_VariableName;

dstring FunctionAtom() {
	return "(?:⎕UCS|"d ~  //Multiple-char
		"[βγδεζηθικλμξρστφχψΓΔΘΛΞΠΣΦΨΩ!\"$%*+,\\-<=>?|~ý⊢⊣⌷≤≥≠∨∧÷×∊↑↓○⌈⌊∇⊂⊃∩∪⊥⊤⍱⍲⍒⍋⍉⌽⊖⍟⌹⍕⍎⍪≡≢⍷→⍶⍸⍹⍇⊆⊇⌻⌼⍃⍄⍅⍆⍈⍊⍌⍍⍏⍐⍑⍓⍔⍖⍗⍘⍚⍛⍜⍠⍡⍥⍦⍧⍩⍭⍮⍯⍰√⊗ϼ∍⋾ℑℜℂℍℕℙℚℝℤ℗ℼ↘⇘↺´á]|"d ~ //not APL
		"[⍳⍴])"d; //APL
}
alias FunctionAtomRE = memoize!(() => regex("^"d ~ FunctionAtom() ~ "$"d));

bool isFunctionAtom(dstring arg) {
	auto c = matchFirst(arg, FunctionAtomRE);
	return !!c; //check if the match is the entire string	
}
LazyCall parseFunctionAtom(dstring arg) {
	return atom(dealias(arg));
}

dstring FunctionVariable() {
	return "(?:"d ~ LowercaseLetter() ~ VariableTail() ~ ")"d;
}
alias FunctionVariableRE = memoize!(() => regex("^"d ~ FunctionVariable() ~ "$"d));

bool isFunctionVariable(dstring arg) {
	auto c = matchFirst(arg, FunctionVariableRE);
	return !!c; //check if the match is the entire string
}
LazyCall parseFunctionVariable(dstring arg) {
	return atom(arg);
}
