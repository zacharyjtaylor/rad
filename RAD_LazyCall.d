module RAD_LazyCall;

import RAD_Value, RAD_utils;
import std.variant, std.conv, std.functional;

Value activate(LazyCall arg) {
	return arg.activate();
}

Value activate(Value arg) {
	return arg; //TODO: make tis work on arrays
}

class LazyCall { //A lazy call so assignments will work correctly
	public Value delegate(Value[]) fn;
	public Value[] args;
	public dstring rep;
	this(Value delegate(Value[]) fn, Value[] args) {
		this.fn = fn;
		this.args = args;
		this.rep = this.fn.to!dstring;
	}
	this(Value delegate(Value[]) fn, Value[] args, dstring rep) {
		this.fn = fn;
		this.args = args;
		this.rep = rep;
	}
	public Value activate() {
		return this.fn(this.args);
	}
	public T opCast(T)() {
		return this.rep ~  this.args.to!T;
	}
	public LazyCall opCall(Value[] args) {
		return new LazyCall(delegate Value(Value[] argv) {
			return this.activate()(argv);
		}, args, cast(dstring)this);
	}
}

LazyCall lambdify(Value x) { //This makes it so `atom` can return a `LazyCall` even for nilads, s
	return new LazyCall(delegate Value(Value[] args) {
		return x;
	}, [], "λ["d ~ (cast(dstring)(x)) ~ "]"d);
}
LazyCall lambdify(Value x, dstring rep) {
	return new LazyCall(delegate Value(Value[] args) {
		return x;
	}, [], "λ["d ~ rep ~ "]"d);
}

//lambdify(x).fn = (...) => x
