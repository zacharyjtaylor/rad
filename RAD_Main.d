import std.stdio, std.conv, std.variant, std.complex;
import RAD_LazyCall, RAD_Value, RAD_utils, RAD_Atoms;
import RAD_Tokenize, RAD_Evaluate, RAD_Number, RAD_AtomUtils;
import RAD_Variable, RAD_Unicode;

Value run(dstring arg) {
	auto ev = evaluate(tokenize(arg));
	LazyCall val;
	if(ev.group == "F"d && RADNamespace.length > 0) {
		if("α"d in RADNamespace[0]) {
			val = ev.value([getVariable_("α"d), getVariable_("ω"d)]); //program is called dyadically
		} else if("ω"d in RADNamespace[0]) {
			val = ev.value([getVariable_("ω"d)]); //program is called monadically
		} else {
			val = ev.value;
		}
	} else {
		val = ev.value;
	}
	return activate(val);
	
}

void main(string[] args) {
	bool flag_u;
	bool flag_e;
	string tmp;
	dstring code;
	string usage = "Usage:\nRAD f <file> [args] Reads file <file> as a RAD program (using the RAD codepage), with arguments [args].\n" ~
		"RAD fu <file> [args] Reads <file> as a RAD program (using unicode), with arguments [args].\n" ~
		"RAD e <code> [args] Use <code> as a RAD program (using the RAD codepage), with arguments [args].\n" ~
		"RAD eu <code> [args] Use <code> as a RAD program (using unicode), with arguments [args].\n" ~
		"RAD ru Run a REPL session (using unicode).\n" ~
		"RAD r Run a REPL session (using the RAD codepage) [Probably bugged].\n";

	Value[] argv;

	if(args.length == 2) {
		if(args[1] == "ru") {
			initRAD();
			dstring res = readln!dstring()[0 .. $-1];
			while(res != ""d) {
				auto val = run(fromASCII(res));
				writeln(repr(val));
				res = readln!dstring()[0 .. $-1];
			}
			return;
		}
		if(args[1] == "r") {
			initRAD();
			string res = readln!string()[0 .. $-1];
			while(res != "") {
				auto val = run(toUnicode(res));
				writeln(repr(val));
				res = readln!string()[0 .. $-1];
			}
			return;
		}
		writeln(usage);
		return;
	}

	if(args.length < 3) {
		writeln(usage);
		return;
	}
	switch(args[1]) {
		case "eu":
			flag_u = true;
			flag_e = true;
			break;
		case "e":
			flag_e = true;
			break;
		case "fu":
			flag_u = true;
			break;
		case "f":
			break;
		default:
			writeln(usage);
			return;
			break;
	}

	if(args.length == 3) { //Initialize system variables and arguments.
		initRAD();
	} else {
		string[] arr = args[3 .. $];
		foreach(i; arr) {
			argv ~= run(i.to!dstring); //NOTE: No variables and stuff should be done in the arguments
		}
		initRAD(argv);
	}

	if(!flag_e) {
		auto F = File(args[2], "r");
		string line = F.readln();
		while(line.length > 0) { //Loop until we reach the end of the file.
			tmp ~= line;
			line = F.readln();
		}
		F.close();
		while(tmp.length > 0 && tmp[$ - 1] == '\n') { //Remove trailing new lines. 
			tmp = tmp[0 .. $ - 1];
		}
	} else {
		tmp = args[2];
	}

	if(!flag_u) {
		code = toUnicode(tmp);
	} else {
		code = fromASCII(tmp.to!dstring);
	}

	auto val = run(code);
	writeln(repr(val, true));
}
