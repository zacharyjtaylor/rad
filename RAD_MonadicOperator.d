module RAD_MonadicOperator;

import std.regex, std.functional;
import RAD_Value, RAD_LazyCall, RAD_Atoms;
import RAD_Alias, RAD_VariableName;


dstring MonadicOperatorAtom() {
	return "(?:[&/\\\\⌶¨⍨⌿⍀⍫⌸⍁⍂ϝ⌾ō⍭∥‽]|"d ~ //1-char
		"⎕MISMATCH)"d; // multi-char
}
alias MonadicOperatorAtomRE = memoize!(() => regex("^"d ~ MonadicOperatorAtom() ~ "$"d));

bool isMonadicOperatorAtom(dstring arg) {
	auto c = matchFirst(arg, MonadicOperatorAtomRE);
	return !!c; //check if the match is the entire string	
}
LazyCall parseMonadicOperatorAtom(dstring arg) {
	return atom(dealias(arg));
}

dstring MonadicOperatorVariable() {
	return "(?:∆"d ~ VariableTail() ~ ")"d;
}
alias MonadicOperatorVariableRE = memoize!(() => regex("^"d ~ MonadicOperatorVariable() ~ "$"d));

bool isMonadicOperatorVariable(dstring arg) {
	auto c = matchFirst(arg, MonadicOperatorVariableRE);
	return !!c; //check if the match is the entire string
}
LazyCall parseMonadicOperatorVariable(dstring arg) {
	return atom(arg);
}
