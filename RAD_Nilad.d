module RAD_Nilad;

import std.regex, std.functional;
import RAD_Number, RAD_String, RAD_Value;
import RAD_LazyCall, RAD_Atoms, RAD_VariableName;
import RAD_Alias;

dstring NiladicAtom() {
	return "(?:¯∞|⎕IO|"d ~ //Multiple-char
		"[απω⍬∞¯⎕⍞"d ~ //1-char (not APL)
		"⍺⍵])"d; //1-char APL
}
alias NiladicAtomRE = memoize!(() => regex("^"d ~ NiladicAtom() ~ "$"d));

bool isNiladicAtom(dstring arg) {
	auto c = matchFirst(arg, NiladicAtomRE);
	return !!c; //check if the match is the entire string	
}
LazyCall parseNiladicAtom(dstring arg) {
	return atom(dealias(arg));
}

dstring NiladicVariable() {
	return "(?:"d ~ UppercaseLetter() ~ VariableTail() ~ ")"d;
}
alias NiladicVariableRE = memoize!(() => regex("^"d ~ NiladicVariable() ~ "$"d));

bool isNiladicVariable(dstring arg) {
	auto c = matchFirst(arg, NiladicVariableRE);
	return !!c; //check if the match is the entire string
}
LazyCall parseNiladicVariable(dstring arg) {
	return atom(arg);
}
