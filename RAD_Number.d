module RAD_Number;

import std.regex, std.conv, std.string, std.array, std.complex, std.functional;
import RAD_utils;

alias NumberType = Complex!double;
alias D = to!dstring;

/* NOTE: Complex!double is used as the type for numbers, as it encompasses all number types, output will be formatted depending on value */

dstring Number() {
	dstring _Number = ""d;
	for(int i = 36; i > 0; i--) { //Our base is `i`, in the range 1-36 inclusive.
		dstring Digit = i<=10?"[0-"d~text(i-1).D~"]"d:"[0-9a-"d~cast(dchar)(int('a')+i-11)~"A-"d~cast(dchar)(int('A')+i-11)~"]".D; //Possible digits
		dstring Power = i<15?`[eE\^]`d:`\^`d; //Separator used for scientific notation
		dstring Postfix = "_"d ~ text(i).D; //Base postfix
		dstring Integer = `(?:¯?`d ~ Digit ~ "+)"d; //Integers
		dstring Float = "(?:(?:"d ~ Integer ~ `\.`d ~ Digit ~ `*)|(?:¯?\.`d ~ Digit ~ "+))"d; //Floating point
		dstring Expand = "(?:"d ~ Integer ~ "|"d ~ Float ~ ")"d; //Expanded (not scientific notation)
		dstring Sci = "(?:"d ~ Expand ~ Power ~ Integer ~ ")"d; //Scientific notataion
		_Number ~= "|(?:(?:"d ~ Expand ~ "|"d ~ Sci ~ ")"d ~ Postfix ~ "[iI]?)"d; //Append the current base to the regex of possible numbers
	}
	_Number = _Number[1..$];
	//_Number = "(?:(?=.*_)(?:"d ~ _Number ~ "))"d; //Removed it for now, hopefully it doesn't break anything.
	{ //Now do it for normal decimal numbers
		dstring Digit = "[0-9]"d;
		dstring Power = `[eE\^]`d;
		dstring Postfix = ``d;
		dstring Integer = `(?:¯?`d ~ Digit ~ "+)"d;
		dstring Float = "(?:(?:"d ~ Integer ~ `\.`d ~ Digit ~ `*)|(?:¯?\.`d ~ Digit ~ "+))"d;
		dstring Expand = "(?:"d ~ Float ~ "|"d ~ Integer ~ ")"d;
		dstring Sci = "(?:"d ~ Expand ~ Power ~ Integer ~ ")"d;
		_Number ~= "|(?:(?:"d ~ Expand ~ "|"d ~ Sci ~ ")"d ~ Postfix ~ "[iI]?)"d;
	}
	_Number = "(?:"d ~ _Number ~ ")"d;
	return _Number;
}
alias NumberRE = memoize!(() => regex("^"d ~ Number() ~ "$"d)); //memoize so it only has to be generated once

bool isNumber(dstring arg) { //Checks if a dstring is a number
	auto c = matchFirst(arg, NumberRE);
	return !!c; //check if the match is the entire string
}

int convertFromBase(dstring arg, int base) { //Converts base `base` number to an integer/
	int[dchar] bv; //Values of letters in bases
	for(int i = 0; i <= 9; i++) { //1-9
		bv[(i.text.D)[0]] = i;
	}
	for(int i = 0; i < 26; i++) {
		bv[cast(dchar)(int('a') + i)] = 10 + i; //a-z
		bv[cast(dchar)(int('A') + i)] = 10 + i; //A-Z
	}
	int res = 0;
	for(int i = 0; i < arg.length; i++) {
		res *= base; 
		res += bv[arg[i]];
	}
	return res;
}

int convertFromBase(dstring arg, NumberType base) {
	return arg.convertFromBase(base.re.to!int);
}

NumberType parseNumber(dstring arg) {
	if(arg[0] == '¯') { //Negatives
		return -parseNumber(arg[1 .. $]);
	}
	if(arg[$ - 1] == 'i' || arg[$ - 1] == 'I') { //Imaginary numbers
		return NumberType(0.0, 1.0) * parseNumber(arg[0 .. $ - 1]);
	}
	if(!arg.contains('_')) { //Normal decimal numbers
		return parseNumber(arg ~ "_10");
	}
	dstring num = arg.split("_")[0]; //The actual number portion
	int base = arg.split("_")[1].to!int; //The base

	if(num.contains('^')) { //Scientific notation
		return parseNumber(num.split("^")[0]~"_"d~text(base).D) * ((NumberType(base.to!double, 0.0)) ^^ parseNumber(num.split("^")[1]~"_"d~text(base).D));
	} else if(num.contains('e') && base < 15) {
		return parseNumber(num.split("e")[0]~"_"d~text(base).D) * ((NumberType(base.to!double, 0.0)) ^^ parseNumber(num.split("e")[1]~"_"d~text(base).D));
	} else if(num.contains('E') && base < 15) {
		return parseNumber(num.split("E")[0]~"_"d~text(base).D) * ((NumberType(base.to!double, 0.0)) ^^ parseNumber(num.split("E")[1]~"_"d~text(base).D));
	}

	dstring[2] temp = (num.split(".") ~ [""d])[0 .. 2]; //Floating point and integers
	if(temp[0].length == 0) {
		temp[0] = "0"d;
	}
	if(temp[1].length == 0) {
		temp[1] = "0"d;
	}
	
	NumberType left = NumberType(temp[0].convertFromBase(base).to!double, 0.0);
	NumberType right = NumberType(temp[1].convertFromBase(base).to!double, 0.0);
	return left + right * NumberType(base.to!double, 0.0) ^^ NumberType(-temp[1].length.to!double, 0.0);
}
