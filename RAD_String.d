module RAD_String;

import std.regex, std.functional;
import RAD_Value;

dstring String() {
	dstring Nonquote = "(?:''|[^'])"d; //Any character inside a quote
	dstring String_ = "(?:'"d ~ Nonquote ~ "*')"d; //A string
	return String_;
}
alias StringRE = memoize!(() => regex("^"d ~ String() ~ "$"d));

bool isString(dstring arg) {
	auto c = matchFirst(arg, StringRE);
	return !!c; //check if the match is the entire string
}	
Value parseString(dstring arg) {
	//A "string" can either be a character or a string
	dchar[] res;
	for(int i = 1; i < arg.length - 1; i++) {
		res ~= [arg[i]];
		if(arg[i] == '\'') {
			i++;
		}
	}
	if(res.length == 1) {
		return Value(res[0]);
	}
	return Value(res);
}
