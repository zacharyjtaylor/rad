module RAD_Tokenize;

import std.variant, std.regex, std.stdio;
import std.functional, std.complex;
import RAD_Value, RAD_LazyCall, RAD_Atoms;
import RAD_Number, RAD_String, RAD_Nilad;
import RAD_Function, RAD_MonadicOperator, RAD_DyadicOperator;
import RAD_Comment, RAD_WhiteSpace;

struct Segment {
	public dstring group;
	public LazyCall value;
	public dstring rep;

	this(dstring group, LazyCall value, dstring rep) {
		this.group = group;
		this.value = value;
		this.rep = rep;
	}
	this(T)(dstring group, T value, dstring rep) {
		this.group = group;
		this.value = lambdify(Value(value), rep);
		this.rep = rep;
	}
	public T opCast(T)() {
		return "Segment(" ~ group ~ ", "d ~ (cast(dstring)this.value) ~ ", "d ~ rep ~ ")"d;
	}
}

dstring Expression() {
	return Comment() ~ "|"d ~
		DyadicOperatorAtom() ~ "|"d ~
		DyadicOperatorVariable() ~ "|"d ~
		FunctionAtom() ~ "|"d ~
		FunctionVariable() ~ "|"d ~
		MonadicOperatorAtom() ~ "|"d ~ 
		MonadicOperatorVariable() ~ "|"d ~
		RAD_Number.Number() ~ "|"d ~
		NiladicAtom() ~ "|"d ~
		NiladicVariable() ~ "|"d ~
		String() ~ "|"d ~
		WhiteSpace() ~ "|.|\\n"d;
}
alias ExpressionRE = memoize!(() => regex(Expression()));

Segment[] tokenize(dstring arg) {
	Segment[] res;
	auto matches = matchAll(arg, ExpressionRE);
	while(!matches.empty()) {
		dstring section = matches.hit();
		if(isNumber(section)) {
			res ~= Segment("N"d, parseNumber(section), section);
		} else if(isString(section)) {
			res ~= Segment("N"d, parseString(section), section);
		} else if(isNiladicAtom(section)) {
			res ~= Segment("N"d, parseNiladicAtom(section), section);
		} else if(isNiladicVariable(section)) {
			res ~= Segment("N"d, parseNiladicVariable(section), section);
		} else if(isFunctionAtom(section)) {
			res ~= Segment("F"d, parseFunctionAtom(section), section);
		} else if(isFunctionVariable(section)) {
			res ~= Segment("F"d, parseFunctionVariable(section), section);
		} else if(isMonadicOperatorAtom(section)) {
			res ~= Segment("M"d, parseMonadicOperatorAtom(section), section);
		} else if(isMonadicOperatorVariable(section)) {
			res ~= Segment("M"d, parseMonadicOperatorVariable(section), section);
		} else if(isDyadicOperatorAtom(section)) {
			res ~= Segment("D"d, parseDyadicOperatorAtom(section), section);
		} else if(isDyadicOperatorVariable(section)) {
			res ~= Segment("D"d, parseDyadicOperatorVariable(section), section);
		} else if(isComment(section) || isWhiteSpace(section)) {
		} else if(section == "\n"d) {
			res ~= Segment("⋄"d, lambdify(Value(0)), section);
		} else if(section == ";"d) {
			Complex!double NAN = Complex!double(double.nan, double.nan);
			res ~= Segment(";"d, lambdify(Value(new Semicolon(Value(NAN), Value(NAN)))), "(;)"d);
		} else if(section.length == 1) {
			res ~= Segment(section, lambdify(Value(0)), section);
		}
		matches.popFront();
	}
	return res;
}
