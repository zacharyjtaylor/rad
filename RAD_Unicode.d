module RAD_Unicode;

import std.regex, std.conv;

dstring cp = "αβγδεζηθικ⋄λμξπρστφχψωΓΔΘΛΞΠΣΦΨΩ !\"#$%&'()*+,-./0123456789:;<=>?@ABCDEFGHIJKLMNOPQRSTUVWXYZ[\\]^_`abcdefghijklmnopqrstuvwxyz{|}~ý⌶¯⍬⊢∆⍙⊣⌷¨⍨⌿⍀≤≥≠∨∧÷×∊↑↓○⌈⌊∇∘⊂⊃∩∪⊥⊤⍱⍲⍒⍋⍉⌽⊖⍟⌹⍕⍎⍫⍪≡≢⍷←→⍝⎕⍞⍣⍶⍸⍹⌸⌺⍇⦃⦄⍢⍤⍁⍂⊆⊇ϝ⊙⌾⌻⌼⍃⍄⍅⍆⍈⍊⍌⍍⍏⍐⍑⍓⍔⍖⍗⍘⍚⍛⍜⍠⍡⍥⍦⍧⍩⍭⍮⍯⍰√∥‽⊗ϼ∍⋾ℑℜℂℍℕℙℚℝℤ℗ℼ↘⇘↺´ō∞á"d;

dstring toUnicode(string arg) {//Codepage => Unicode
	//FIXME: this isn't working currently, I don't know why though
	dstring res;
	foreach(i; arg) {
		res ~= cp[int(i)];
	}
	return res;
}

int detextify(dstring arg) {
	int[char] dict = ['0':0,'1':1,'2':2,'3':3,'4':4,'5':5,'6':6,'7':7,'8':8,'9':9,'a':10,'A':10,'b':11,'B':11,'c':12,'C':12,'d':13,'D':13,'e':14,'E':14,'f':15,'F':15];
	return 16 * dict[arg[0].to!char] + dict[arg[1].to!char];
}

dstring fromASCII(dstring arg) {//Remove ASCII Aliases
	//Backtics are unimplemented as of right now...
	//Same goes for $
	return replaceAll!(delegate dstring(Captures!dstring x)=>""d ~ cp[x.hit[1 .. 3].detextify])(arg, regex("(#[0-9A-Fa-f][0-9A-Fa-f])"d)); //#XX
}
