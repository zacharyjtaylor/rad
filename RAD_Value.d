module RAD_Value;

import std.variant, std.complex, std.stdio, std.traits, std.conv, std.math;
import std.functional: toDelegate;
import RAD_utils;

alias Number = Complex!double;
alias Value_ = Algebraic!(Number, dchar, Function, Semicolon, This[]);

Number toNumber(T)(T arg) {
	return Number(1.0, 0.0) * arg;
}

struct Value {
	public Value_ theValue; //make it public, so we don't run into problems later
	alias theValue this;
	this(Value arg) {
		this.theValue = arg.theValue;
	}
	this(Value_ delegate(Value_[]) fn) {
		this.theValue = new Function(fn);
	}
	this(Value delegate(Value[]) fn) {
		this.theValue = new Function(delegate Value_(Value_[] args) {
			return fn(args.map(delegate Value(Value_ a)=>Value(a))).theValue;
		});
	}
	this(Value_ function(Value_[]) fn) {
		this.theValue = new Function(toDelegate(fn));
	}
	this(Value function(Value[]) fn) {
		this.theValue = new Function(delegate Value_(Value_[] args) {
			return fn(args.map(delegate Value(Value_ a)=>Value(a))).theValue;
		});
	}
	this(T)(T[0] arg) { //Allow `Value([])` to be a thing
		Value_[] tmp;
		this.theValue = tmp;
	}
	this(T)(T arg) if(isSomeString!(T)) { //Do not allow `int[]` here.
		Value[] tmp;
		foreach(i; arg) {
			dchar j = i;
			tmp ~= Value(j);
		}
		this = Value(tmp);
	}
	this(T)(T[] arr) if(!isSomeString!(T[])) { //Do not allow `string` here.
		Value_[] res;
		for(int i = 0; i < arr.length; i++) {
			res ~= Value(arr[i]).theValue;
		}
		this.theValue = res;
	}
	this(T)(T value) if(isNumeric!(T)) {
		this.theValue = Number(value, 0);
	}
	this(T)(T value) if(!isArray!(T) && !isNumeric!(T)) {
		this.theValue = value;
	}
	public Value opUnary(string s)() if(s == "-") {
		if(this.peek!Number !is null) {
			return Value(-this.get!Number);
		}
		throw new Exception("RAD: INTERNAL ERROR"); 	
	}
	public Value opUnary(string s)() if(s == "+") {
		if(this.peek!Number !is null) {
			return Value(+this.get!Number);
		}
		throw new Exception("RAD: INTERNAL ERROR");
	}
	public Value opUnary(string s)() if(s == "~")  {
		throw new Exception("RAD: INTERNAL ERROR");
	}
	//`*n` should be handled already.
	public Value opUnary(string s)() if(s == "++") {
		throw new Exception("RAD: INTERNAL ERROR");
	}
	public Value opUnary(string s)() if(s == "--") {
		throw new Exception("RAD: INTERNAL ERROR");
	}
	public Value opIndexUnary(string s)(...) {
		throw new Exception("RAD: INTERNAL ERROR");
	}
	//`cast` is not handled by `theValue`
	public T opCast(T)() {
		if(this.peek!Number !is null) {
			return (this.get!Number).to!T;
		}
		if(this.peek!dchar !is null) {
			return (this.get!dchar).to!T;
		}
		if(this.peek!(Value_[]) !is null) {
			return (this.get!(Value_[])).to!T;
		}
		if(this.peek!Function !is null) {
			return cast(T)(this.get!Function);
		}
		if(this.peek!Semicolon !is null) {
			return cast(T)(this.get!Semicolon);
		}
		throw new Exception("RAD: INTERNAL ERROR");
	}

	// +
	public Value opBinary(string s)(Value rhs) if(s == "+") {
		if(this.peek!Number !is null && rhs.peek!Number !is null) {
			return Value(this.get!Number + rhs.get!Number);
		}
		throw new Exception("RAD: INTERNAL ERROR");
	}
	public Value opBinary(string s, T)(T rhs) if(s == "+") {
		if(this.peek!Number !is null) {
			return Value(this.get!Number + rhs.to!Number);
		}
		throw new Exception("RAD: INTERNAL ERROR");
	}
	public Value opBinaryRight(string s, T)(T rhs) if(s == "+") {
		if(this.peek!Number !is null) {
			return Value(rhs.to!Number + this.get!Number);
		}
		throw new Exception("RAD: INTERNAL ERROR");
	}

	// -
	public Value opBinary(string s)(Value rhs) if(s == "-") {
		if(this.peek!Number !is null && rhs.peek!Number !is null) {
			return Value(this.get!Number - rhs.get!Number);
		}
		throw new Exception("RAD: INTERNAL ERROR");
	}
	public Value opBinary(string s, T)(T rhs) if(s == "-") {
		if(this.peek!Number !is null) {
			return Value(this.get!Number - rhs.to!Number);
		}
		throw new Exception("RAD: INTERNAL ERROR");
	}
	public Value opBinaryRight(string s, T)(T rhs) if(s == "-") {
		if(this.peek!Number !is null) {
			return Value(rhs.to!Number - this.get!Number);
		}
		throw new Exception("RAD: INTERNAL ERROR");
	}

	// *
	public Value opBinary(string s)(Value rhs) if(s == "*") {
		if(this.peek!Number !is null && rhs.peek!Number !is null) {
			return Value(this.get!Number * rhs.get!Number);
		}
		throw new Exception("RAD: INTERNAL ERROR");
	}
	public Value opBinary(string s, T)(T rhs) if(s == "*") {
		if(this.peek!Number !is null) {
			return Value(this.get!Number * rhs.to!Number);
		}
		throw new Exception("RAD: INTERNAL ERROR");
	}
	public Value opBinaryRight(string s, T)(T rhs) if(s == "*") {
		if(this.peek!Number !is null) {
			return Value(rhs.to!Number * this.get!Number);
		}
		throw new Exception("RAD: INTERNAL ERROR");
	}

	// /
	public Value opBinary(string s)(Value rhs) if(s == "/") {
		if(this.peek!Number !is null && rhs.peek!Number !is null) {
			return Value(this.get!Number / rhs.get!Number);
		}
		throw new Exception("RAD: INTERNAL ERROR");
	}
	public Value opBinary(string s, T)(T rhs) if(s == "/") {
		if(this.peek!Number !is null) {
			return Value(this.get!Number / rhs.to!Number);
		}
		throw new Exception("RAD: INTERNAL ERROR");
	}
	public Value opBinaryRight(string s, T)(T rhs) if(s == "/") {
		if(this.peek!Number !is null) {
			return Value(rhs.to!Number / this.get!Number);
		}
		throw new Exception("RAD: INTERNAL ERROR");
	}

	// %
	public Value opBinary(string s)(Value rhs) if(s == "%") {
		if(this.peek!Number !is null && rhs.peek!Number !is null) {
			if(this.get!Number.im == 0 && rhs.get!Number.im == 0) {
				return Value(this.get!Number.re % rhs.get!Number.re);
			}
			return this - rhs * Value(Number(floor((this / rhs).get!Number.re), floor((this / rhs).get!Number.im)));
			//a % b = a - b * floor(a / b)
		}
		throw new Exception("RAD: INTERNAL ERROR");
	}
	public Value opBinary(string s, T)(T rhs) if(s == "%") {
		return this % Value(rhs);
	}
	public Value opBinaryRight(string s, T)(T rhs) if(s == "%") {
		return Value(rhs) % this;
	}

	// ^^
	public Value opBinary(string s)(Value rhs) if(s == "^^") {
		if(this.peek!Number !is null && rhs.peek!Number !is null) {
			return Value(this.get!Number ^^ rhs.get!Number);
		}
		throw new Exception("RAD: INTERNAL ERROR");
	}
	public Value opBinary(string s, T)(T rhs) if(s == "^^") {
		if(this.peek!Number !is null) {
			return Value(this.get!Number ^^ rhs.to!Number);
		}
		throw new Exception("RAD: INTERNAL ERROR");
	}
	public Value opBinaryRight(string s, T)(T rhs) if(s == "^^") {
		if(this.peek!Number !is null) {
			return Value(rhs.to!Number ^^ this.get!Number);
		}
		throw new Exception("RAD: INTERNAL ERROR");
	}

	// &
	public Value opBinary(string s)(Value rhs) if(s == "&") {
		throw new Exception("RAD: INTERNAL ERROR");
	}
	public Value opBinary(string s, T)(T rhs) if(s == "&") {
		throw new Exception("RAD: INTERNAL ERROR");
	}
	public Value opBinaryRight(string s, T)(T rhs) if(s == "&") {
		throw new Exception("RAD: INTERNAL ERROR");
	}

	// |
	public Value opBinary(string s)(Value rhs) if(s == "|") {
		throw new Exception("RAD: INTERNAL ERROR");
	}
	public Value opBinary(string s, T)(T rhs) if(s == "|") {
		throw new Exception("RAD: INTERNAL ERROR");
	}
	public Value opBinaryRight(string s, T)(T rhs) if(s == "|") {
		throw new Exception("RAD: INTERNAL ERROR");
	}

	// ^
	public Value opBinary(string s)(Value rhs) if(s == "^") {
		throw new Exception("RAD: INTERNAL ERROR");
	}
	public Value opBinary(string s, T)(T rhs) if(s == "^") {
		throw new Exception("RAD: INTERNAL ERROR");
	}
	public Value opBinaryRight(string s, T)(T rhs) if(s == "^") {
		throw new Exception("RAD: INTERNAL ERROR");
	}

	// <<
	public Value opBinary(string s)(Value rhs) if(s == "<<") {
		throw new Exception("RAD: INTERNAL ERROR");
	}
	public Value opBinary(string s, T)(T rhs) if(s == "<<") {
		throw new Exception("RAD: INTERNAL ERROR");
	}
	public Value opBinaryRight(string s, T)(T rhs) if(s == "<<") {
		throw new Exception("RAD: INTERNAL ERROR");
	}

	// >>
	public Value opBinary(string s)(Value rhs) if(s == ">>") {
		throw new Exception("RAD: INTERNAL ERROR");
	}
	public Value opBinary(string s, T)(T rhs) if(s == ">>") {
		throw new Exception("RAD: INTERNAL ERROR");
	}
	public Value opBinaryRight(string s, T)(T rhs) if(s == ">>") {
		throw new Exception("RAD: INTERNAL ERROR");
	}

	// <<<
	public Value opBinary(string s)(Value rhs) if(s == "<<<") {
		throw new Exception("RAD: INTERNAL ERROR");
	}
	public Value opBinary(string s, T)(T rhs) if(s == "<<<") {
		throw new Exception("RAD: INTERNAL ERROR");
	}
	public Value opBinaryRight(string s, T)(T rhs) if(s == "<<<") {
		throw new Exception("RAD: INTERNAL ERROR");
	}

	// ~
	public Value opBinary(string s)(Value rhs) if(s == "~") {
		if(this.peek!(Value_[]) !is null && rhs.peek!Number !is null) {
			return Value(this.get!(Value_[]) ~ [Value_(rhs.get!Number)]);
		}
		if(this.peek!(Value_[]) !is null && rhs.peek!dchar !is null) {
			return Value(this.get!(Value_[]) ~ [Value_(rhs.get!dchar)]);
		}
		if(this.peek!(Value_[]) !is null && rhs.peek!(Value_[]) !is null) {
			return Value(this.get!(Value_[]) ~ rhs.get!(Value_[]));
		}
		throw new Exception("RAD: INTERNAL ERROR");
	}
	public Value opBinary(string s, T)(T rhs) if(s == "~") {
		if(this.peek!(Value_[]) !is null) {
			return Value(this.get!(Value_[]) ~ rhs);
		}
		throw new Exception("RAD: INTERNAL ERROR");
	}
	public Value opBinaryRight(string s, T)(T rhs) if(s == "~") {
		if(this.peek!Number !is null) {
			return Value(rhs ~ Value(this.get!Number));
		}
		if(this.peek!dchar !is null) {
			return Value(rhs ~ this.get!dchar);
		}
		if(this.peek!(Value_[]) !is null) {
			return Value(rhs ~ this.get!(Value_[]));
		}
		throw new Exception("RAD: INTERNAL ERROR");
	}

	// in
	public Value opBinary(string s)(Value rhs) if(s == "in") {
		throw new Exception("RAD: INTERNAL ERROR");
	}
	public Value opBinary(string s, T)(T rhs) if(s == "in") {
		throw new Exception("RAD: INTERNAL ERROR");
	}
	public Value opBinaryRight(string s, T)(T rhs) if(s == "in") {
		throw new Exception("RAD: INTERNAL ERROR");
	}

	// `==` is somewhat handled by `theValue`.

	// <, <=, >, >=
	public int opCmp(T)(T rhs) {
		if(this.peek!Number !is null) {
			double S = (this - rhs).get!Number.re;
			if(S < 0) {
				return -1;
			} else if(S == 0) {
				return 0;
			} else {
				return 1;
			}
		}
		throw new Exception("RAD: INTERNAL ERROR");
	}

	// ()
	public Value opCall(Value[] args) {
		if(this.peek!(Function) !is null) {
			Value_[] tmp;
			for(int i = 0; i < args.length; i++) {
				tmp ~= args[i].theValue;
			}
			return Value((this.get!Function)()(tmp));
		}
		throw new Exception("RAD: INTERNAL ERROR");
	}

	// `=` is handled by `theValue`

	// []
	public Value opIndex(Value i) {
		if(this.peek!(Value_[]) !is null && i.peek!Number !is null) {
			return Value(this.get!(Value_[])[i.get!Number.re.to!int]);
		}
		throw new Exception("RAD: INTERNAL ERROR");
	}
	public Value opIndex(T)(T i) {
		if(this.peek!(Value_[]) !is null) {
			return Value(this.get!(Value_[])[i]);
		}
		throw new Exception("RAD: INTERNAL ERROR");
	}

	// []=
	public Value opIndexAssign(T, U)(T value, U index) { //TODO: SPEED THIS UP
		if(this.peek!(Value_[]) is null) {
			throw new Exception("RAD: INTERNAL ERROR");
		}
		Value_[] res;
		for(int i = 0; i < this.length; i++) {
			if(i == index) {
				res ~= Value(value).theValue;
			} else {
				res ~= this[i];
			}
		}
		if(index == this.length) {
			res ~= Value(value).theValue;
		}
		this = Value(res);
		return Value(value);
	}
}

class Function {
	public Value_ delegate(Value_[]) fn;
	public dstring rep;
	alias fn this;
	this(Value_ delegate(Value_[]) fn) {
		this.fn = fn;
		this.rep = fn.to!dstring;
	}
	this(Value_ delegate(Value_[]) fn, dstring rep) {
		this.fn = fn;
		this.rep = rep;
	}
	
	public Value_ opCall(Value_[] args) {
		return this.fn(args);
	}

	public T opCast(T)() {
		return this.rep.to!T;
	}
}

class Semicolon {
	public Value A;
	public Value B;
	this(Value a, Value b) {
		this.A = a;
		this.B = b;
	}
	public T opCast(T)() {
		return (cast(T)"("d) ~ (cast(T)this.A) ~ (cast(T)";"d) ~ (cast(T)this.B) ~ (cast(T)")"d);
	}
	public Value[] flatten() {
		Value[] left;
		Value[] right;
		if(this.A.peek!Semicolon !is null) {
			left = this.A.get!Semicolon.flatten();
		} else {
			left = [this.A];
		}
		if(this.B.peek!Semicolon !is null) {
			right = this.B.get!Semicolon.flatten();
		} else {
			right = [this.B];
		}
		return left ~ right;
	}
}
