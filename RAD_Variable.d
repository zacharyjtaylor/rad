module RAD_Variable;

import std.stdio, std.conv;
import RAD_Value, RAD_utils;
import RAD_AtomUtils, RAD_Atoms, RAD_LazyCall, RAD_Evaluate, RAD_Tokenize;

alias Scope = Value[dstring]; //A single scope
alias Namespace = Scope[]; //A combination of scopes, signifying nested dfns/dops.
//The most recently created scope is stored at the end of the array.
//This is also the deepest scope.

Namespace RADNamespace; //The variables stored in the RAD program.

Value getVariable_(dstring name) { //Function to retrieve a variable
	if(name == "⎕"d) {
		return activate(evaluate(tokenize(removeNewline(readln!dstring))).value);
	}
	if(name == "⍞"d) {
		return Value(removeNewline(readln!dstring));
	}
	foreach_reverse(sco; RADNamespace) { //Find the most recently created scope
		if(name in sco) { //That contains `name`
			return sco[name]; //Then return the value `name` holds there.
		}
	}
	throw new Exception("RAD: VALUE ERROR: " ~ name.to!string); //Otherwise, raise a VALUE ERROR.
}
Value getVariable(Value[] name) { //A version of the abbove function that works with LazyCall
	dstring tmp;
	foreach(c; name[0].get!(Value_[])) {
		tmp ~= c.get!dchar;
	}
	return getVariable_(tmp);
}


void setVariable(dstring name, Value value) {
	//TODO: allow for `;`s that are empty.
	//TODO: allow A[B1][B2]...
	//TODO: allow A B←X Y
	if(name == "⎕"d) {
		writeln(repr(value, true));
		return;
	}
	if(name == "⍞"d) {
		write(repr(value, true));
		return;
	}
	if(name == "0"d) {
		return; //Just for `groupify`
	}
	int sep = -1;
	for(int i = 0; i < name.length; i++) {
		if(name[i] == dchar('['))
			sep = i;
	}
	if(sep != -1) { //Indexed assignment: A[B]←C
		dstring var = name[0 .. sep]; //The variable
		dstring ind = name[sep .. $]; //The index
		Value E = Value([activate(evaluate(tokenize("(ε"d ~ var ~ ")"d ~ ind)).value)]); //(εA)[B]
		Value[] SA; Value[] SC; //Temporary variables for assignment and stuff.
		vectorizeMonadic(delegate Value(Value[] args) {
			SA ~= args[0];
			return Value(0);
		}, 1, E);
		auto D = vectorizeMonadic(delegate Value(Value[] args) {
			return Value(1);
		}, 1, E[0]);
		vectorizeMonadic(delegate Value(Value[] args) {
			SC ~= args[0];
			return Value(0);
		}, 1, enumerate(D));
		Value res = activate(evaluate(tokenize(var)).value);
		for(int i = 0; i < SA.length; i++) {
			Value z = Value(value);
			foreach(Value_ j; SC[i].get!(Value_[])) {
				z = z[Value(j) - getIO()];
			}
			res = setIndex(res, SA[i], z);
		}
		setVariable(var, res);
	} else {
		RADNamespace[$ - 1][name] = value; //This only overrides the most recently created scope.
	}
}


void newScope(Value[] args = []) { //Creates a new scope (when a dfn/dop is called, or the program starts)
	Scope local;
	RADNamespace ~= [local];
	if(args.length == 1) { //Only one argument given
		setVariable("ω"d, args[0]); //First argument is omega
	}
	if(args.length == 2) { //Two arguments given
		setVariable("α"d, args[0]); //First argument is alpha
		setVariable("ω"d, args[1]); //Second argument is omega
	}
	
}

void destroyScope() { //Destroys the most recently created scope (after a dfn/dop is called)
	RADNamespace = RADNamespace[0 .. $ - 1];
}

void initRAD(Value[] args = []) {
	newScope(args); //Make it initially empty with one scope: the global scope.
	setVariable("⎕IO"d, Value(1)); //Index origin defaults to `1`.
}

Value getIO() { //This is used often enough to warrant its own function
	return getVariable_("⎕IO"d);
}
