module RAD_VariableName;

dstring UppercaseLetter() {
	return "[A-Z]"d;
}

dstring LowercaseLetter() {
	return "[a-z]"d;
}

dstring Other() {
	return "[0-9∆⍙]"d;
}

dstring VariableTail() {
	return "(?:"d ~ UppercaseLetter ~ "|"d ~ LowercaseLetter ~ "|"d ~ Other ~ ")*"d;
}
