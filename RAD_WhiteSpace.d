module RAD_WhiteSpace;

import std.regex, std.functional;

dstring WhiteSpace() {
	return "(?:(?: |\\t)+)"d;
}
alias WhiteSpaceRE = memoize!(() => regex("^"d ~ WhiteSpace() ~ "$"d));

bool isWhiteSpace(dstring arg) {
	auto c = matchFirst(arg, WhiteSpaceRE);
	return !!c; //check if the match is the entire string	
}
