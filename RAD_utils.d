module RAD_utils;
import std.conv;
import RAD_Tokenize, RAD_Atoms, RAD_Value, RAD_Evaluate;
//General utilities.

bool contains(dstring s, dchar c) {
	foreach(i;s)
		if(c == i) return true;
	return false;
}

bool contains(T)(T[] s, T c) {
	foreach(i;s)
		if(c == i) return true;
	return false;
}

U[] map(T, U)(T[] arr, U delegate(T) f) {
	U[] res;
	for(int i = 0; i < arr.length; i++) {
		res ~= f(arr[i]);
	}
	return res;
}

U[] mapRTL(T, U)(T[] arr, U delegate(T) f) {
	U[] res;
	foreach_reverse(el; arr) {
		res = [f(el)] ~ res;
	}
	return res;
}

void collapse(T, U)(ref T[] arr, U left, U right, T[] val) { //Replaces 
	arr = arr[0 .. left] ~ val ~ arr[right .. $];
}

bool isAll(T)(T[] arg, T el) {
	foreach(i;arg)
		if(el != i) return false;
	return true;
}

dstring join(dstring sep, dstring[] arr) {
	dstring res = arr[0];
	foreach(i;arr[1..$]) {
		res ~= sep ~ i;
	}
	return res;
}

int indexOf(T)(T[] arr, T el) { //A helper function for `a`i`w
	for(int i = 0; i < arr.length; i++) {
		if(arr[i] == el)
			return i;
	}
	return arr.length.to!int;
}

dstring groupify(Segment[] args) {
	Segment[] tmp;
	foreach(i; args) {
		if(i.group == "N"d) {
			tmp ~= Segment("N"d, Value(0), "0"d);
		} else if(i.group == "F"d) {
			tmp ~= Segment("F"d, atom("+"d), "+"d);
		} else if(i.group == "M"d) {
			tmp ~= Segment("M"d, atom("/"d), "/"d);
		} else if(i.group == "D"d) {
			tmp ~= Segment("D"d, atom("∘"d), "∘"d); //FIXME: THIS COMMAND WILL ERROR ON CERTAIN INPUTS.
		} else {
			tmp ~= Segment(i.group, Value(0), i.group);
		}
	}
	return evaluate(tmp).group;
}
